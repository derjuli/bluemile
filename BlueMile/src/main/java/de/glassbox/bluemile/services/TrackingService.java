package de.glassbox.bluemile.services;

import android.content.Context;
import android.content.Intent;
import android.location.Location;
import android.os.Binder;
import android.os.Bundle;
import android.os.IBinder;
import android.os.PowerManager;
import android.util.Log;

import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.GooglePlayServicesClient;
import com.google.android.gms.location.LocationClient;
import com.google.android.gms.location.LocationRequest;
import com.google.inject.Inject;
import com.squareup.otto.Bus;

import java.util.Date;

import de.glassbox.bluemile.model.BMLocation;
import de.glassbox.bluemile.model.BMTrackResult;
import de.glassbox.bluemile.model.events.BMTrackingUpdateEvent;
import de.glassbox.bluemile.model.events.LocationEvent;
import de.glassbox.bluemile.persistence.DataAccess;
import roboguice.service.RoboService;

/**
 * Created by Juli on 17.05.14.
 */
public class TrackingService extends RoboService implements GooglePlayServicesClient.ConnectionCallbacks, GooglePlayServicesClient.OnConnectionFailedListener,
    com.google.android.gms.location.LocationListener {

    // Update frequency in seconds
    public static final int UPDATE_INTERVAL_IN_SECONDS = 5;
    private static final String TAG = TrackingService.class.getSimpleName();
    private static final String LOCK_NAME_STATIC = "de.glassbox.bluemile.TrackingService";
    // Milliseconds per second
    private static final int MILLISECONDS_PER_SECOND = 1000;
    // Update frequency in milliseconds
    private static final long UPDATE_INTERVAL = MILLISECONDS_PER_SECOND * UPDATE_INTERVAL_IN_SECONDS;
    // The fastest update frequency, in seconds
    private static final int FASTEST_INTERVAL_IN_SECONDS = 1;
    // A fast frequency ceiling in milliseconds
    private static final long FASTEST_INTERVAL = MILLISECONDS_PER_SECOND * FASTEST_INTERVAL_IN_SECONDS;
    // Data
    private static boolean sIsRunning = false;
    private static volatile PowerManager.WakeLock lockStatic = null;
    // Binder given to clients
    private final IBinder mBinder = new TrackingServiceBinder();
    // Define an object that holds accuracy and frequency parameters
    private LocationRequest mLocationRequest;
    private LocationClient mLocationClient;
    private boolean mUpdatesRequested;

    private Location mLastLocation;
    private double mTotalDistanceMeters;
    private int mCount;
    private BMTrackResult mActiveTrackResult;

    @Inject
    private Bus mEventBus;
    @Inject
    private DataAccess mDataAccess;
    //@Inject
    private MockLocationProvider mMockLocationProvider;

    /**
     * Acquires a wake lock so that location updates are always processed.
     *
     * @param context Context of the service.
     * @return A new WakeLock.
     */
    synchronized private static PowerManager.WakeLock getLock(Context context) {
        if (lockStatic == null) {
            PowerManager mgr = (PowerManager) context.getApplicationContext().getSystemService(Context.POWER_SERVICE);

            lockStatic = mgr.newWakeLock(PowerManager.PARTIAL_WAKE_LOCK, LOCK_NAME_STATIC);
            lockStatic.setReferenceCounted(true);
        }

        return (lockStatic);
    }

    public static boolean isRunning() {
        return sIsRunning;
    }

    /*
         * Called by Location Services when the request to connect the
         * client finishes successfully. At this point, you can
         * request the current location or start periodic updates
         */
    @Override
    public void onConnected(Bundle dataBundle) {
        initLocationClient();
        mLocationClient.requestLocationUpdates(mLocationRequest, this);
    }

    /*
     * Called by Location Services if the connection to the
     * location client drops because of an error.
     */
    @Override
    public void onDisconnected() {
        // Display the connection status
        //Toast.makeText(this, "Disconnected. Please re-connect.", Toast.LENGTH_SHORT).show();
        mLastLocation = null;
    }

    /*
     * Called by Location Services if the attempt to
     * Location Services fails.
     */
    @Override
    public void onConnectionFailed(ConnectionResult connectionResult) {
        /*
         * Google Play services can resolve some errors it detects.
         * If the error has a resolution, try sending an Intent to
         * start a Google Play services activity that can resolve
         * error.
         */
        //        if (connectionResult.hasResolution()) {
        //            try {
        //                // Start an Activity that tries to resolve the error
        //                //connectionResult.startResolutionForResult(this, CONNECTION_FAILURE_RESOLUTION_REQUEST);
        //                /*
        //                * Thrown if Google Play services canceled the original
        //                * PendingIntent
        //                */
        //            } catch (IntentSender.SendIntentException e) {
        //                // Log the error
        //                e.printStackTrace();
        //            }
        //        } else {
        //            /*
        //             * If no resolution is available, display a dialog to the
        //             * user with the error.
        //             */
        //            showErrorDialog(connectionResult.getErrorCode());
        //        }
    }

    @Override
    public void onLocationChanged(Location location) {
        // Report to the UI that the location was updated
        if (mLastLocation == null) {
            mLastLocation = location;
            mEventBus.post(new LocationEvent(new BMLocation(location, 0)));
            if (!mUpdatesRequested) {
                mLocationClient.disconnect();
            }
            return;
        }
        if (!mUpdatesRequested) {
            return;
        }

        // calculate travelled distance and speed
        double distance = mLastLocation.distanceTo(location);
        if (mCount++ > 0) {
            mTotalDistanceMeters += distance;
        }
        float timeInSeconds = ((float) (location.getTime() - mLastLocation.getTime())) / 1000f;

        // send speed to ui to update speedometer
        int speed = (int) (distance / timeInSeconds * 3.6f);
        updateCurrentTour(location, speed, mTotalDistanceMeters);
        mLastLocation = location;
    }

    @Override
    public void onCreate() {
        super.onCreate();
        Log.i(TAG, "Service Started.");
        mLocationClient = new LocationClient(this, this, this);
        mLocationClient.connect();
        sIsRunning = true;
    }

    @Override
    public IBinder onBind(Intent intent) {
        Log.d(TAG, "Binding service");
        return mBinder;
    }

    @Override
    public int onStartCommand(Intent intent, int flags, int startId) {
        Log.i(TAG, "Received start id " + startId + ": " + intent);

        PowerManager.WakeLock lock = getLock(this.getApplicationContext());
        if (!lock.isHeld() || (flags & START_FLAG_REDELIVERY) != 0) {
            lock.acquire();
        }

        return START_FLAG_REDELIVERY; // Run until explicitly stopped.
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        Log.i(TAG, "Service Stopped.");
        sIsRunning = false;
        if (mLocationClient != null && mLocationClient.isConnected()) {
            mLocationClient.removeLocationUpdates(this);
        }
    }

    private void initLocationClient() {
        mLocationRequest = LocationRequest.create();
        // Use high accuracy
        mLocationRequest.setPriority(LocationRequest.PRIORITY_HIGH_ACCURACY);
        // Set the update interval to 5 seconds
        mLocationRequest.setInterval(UPDATE_INTERVAL);
        // Set the fastest update interval to 1 second
        mLocationRequest.setFastestInterval(FASTEST_INTERVAL);
    }

    /**
     * Creates a new tour for the given road and stores it in the database.
     */
    public void startTracking(BMTrackResult trackResult) {
        getLock(getApplicationContext()).acquire();
        mUpdatesRequested = true;
        mLastLocation = null;
        mTotalDistanceMeters = 0;
        mLocationClient.connect();

        mActiveTrackResult = trackResult;

        if (mMockLocationProvider != null) {
            // load random track
            mMockLocationProvider.loadRandomRoute();
        }


        mEventBus.post(new BMTrackingUpdateEvent(0, 0, null));
        //mTimer.scheduleAtFixedRate(new LocationUpdateTask(), 500, sLocationUpdateTime);
    }

    public void stopTracking() {
        mActiveTrackResult.setEndTime(new Date());
        mDataAccess.storeTrackResultAsync(mActiveTrackResult, null, false);
        mLocationClient.disconnect();
    }

    /**
     * Creates a new location point and stores it in the database.
     *
     * @param location            The current location from location manager.
     * @param speed               The current speed.
     * @param totalDistanceMeters Total distance in Meters.
     */
    private void updateCurrentTour(Location location, int speed, double totalDistanceMeters) {
        BMLocation loc = new BMLocation(location, speed);
        mDataAccess.storeLocationAsync(loc, mActiveTrackResult.getLocalId());
        mEventBus.post(new BMTrackingUpdateEvent(speed, totalDistanceMeters, loc));
    }

    /**
     * Class used for the client Binder.  Because we know this service always
     * runs in the same process as its clients, we don't need to deal with IPC.
     */
    public class TrackingServiceBinder extends Binder {
        public TrackingService getService() {
            // Return this instance of TrackingService so clients can call public methods
            return TrackingService.this;
        }
    }
}
