package de.glassbox.bluemile.services;

import android.content.Intent;
import android.location.Address;
import android.location.Geocoder;
import android.os.Binder;
import android.os.IBinder;
import android.util.Log;

import com.google.inject.Inject;
import com.squareup.otto.Bus;

import java.io.IOException;
import java.util.List;
import java.util.Locale;

import de.glassbox.bluemile.App;
import de.glassbox.bluemile.model.BMLocation;
import de.glassbox.bluemile.model.BMTrackResult;
import de.glassbox.bluemile.model.events.TrackingEvaluationEvent;
import de.glassbox.bluemile.persistence.DataAccess;
import roboguice.service.RoboService;

/**
 * Created by Juli on 10.06.2014.
 */
public class TrackingEvaluationService extends RoboService {

    public static final String TRACKING_ID = "tracking_id";
    private static final String TAG = TrackingEvaluationService.class.getSimpleName();
    private static final int MAX_SPEED_POINTS = 10;
    // Binder given to clients
    private static boolean sIsRunning = false;
    private final IBinder mBinder = new TrackingEvaluationServiceBinder();
    @Inject
    private DataAccess mDataAccess;
    @Inject
    private Bus mBus;
    private EvaluationThread mEvaluationThread;

    public static boolean isRunning() {
        return sIsRunning;
    }

    @Override
    public IBinder onBind(Intent intent) {
        return mBinder;
    }

    @Override
    public void onCreate() {
        super.onCreate();
        sIsRunning = true;
    }

    @Override
    public synchronized void onDestroy() {
        super.onDestroy();
        sIsRunning = false;
        if (mEvaluationThread != null) {
            mEvaluationThread.interrupt();
        }
    }

    public void processTrackResult(long trackID) {
        mEvaluationThread = new EvaluationThread(trackID);
        mEvaluationThread.start();
    }

    private static class BasicAddress {
        String street;
        String city;
    }

    class EvaluationThread extends Thread {
        long trackID;

        EvaluationThread(long trackID) {
            this.trackID = trackID;
        }

        @Override
        public void run() {
            BMTrackResult trackResult = processTrackResult(trackID);
            if (trackResult != null) {
                mDataAccess.storeTrackResult(trackResult);

                // fake long running evaluation
                trackResult.setTrackStatus(BMTrackResult.STATUS_CREATED);
                for (int i = 0; i < 6; i++) {
                    try {
                        if (i == 5) {
                            trackResult.setTrackStatus(BMTrackResult.STATUS_EVALUATION_DONE);
                        }
                        mBus.post(new TrackingEvaluationEvent(trackResult));
                        Thread.sleep(1500);
                    } catch (InterruptedException e) {
                        e.printStackTrace();
                        trackResult.setTrackStatus(BMTrackResult.STATUS_EVALUATION_DONE);
                        mBus.post(new TrackingEvaluationEvent(trackResult));
                    }
                }
            }
        }

        private BMTrackResult processTrackResult(long trackID) {
            BMTrackResult trackResult = mDataAccess.getTrackResultById(trackID);
            if (trackResult != null) {
                List<BMLocation> locations = mDataAccess.getLocations(trackResult.getLocalId(), Integer.MAX_VALUE);
                if (locations.size() > 0) {

                    int totalDistanceMeters = 0;
                    int maxSpeed = 0;
                    int totalSpeed = 0;
                    int[] speedBuckets = new int[Math.max(1, Math.min(MAX_SPEED_POINTS, locations.size() / 4))];
                    int[] speedBucketsCount = new int[speedBuckets.length];
                    int speedBucketDistance = (int) Math.max(1, Math.ceil(locations.size() / (float) speedBuckets.length)); // prevents divide by zero
                    BMLocation lastLocation = null;
                    int counter = 0;

                    for (BMLocation location : locations) {
                        if (lastLocation != null) {
                            totalDistanceMeters += lastLocation.distanceTo(location);
                        }
                        maxSpeed = location.getSpeed() > maxSpeed ? location.getSpeed() : maxSpeed;

                        int bucket = counter / speedBucketDistance;
                        speedBuckets[bucket] += location.getSpeed();
                        speedBucketsCount[bucket] += 1;
                        totalSpeed += location.getSpeed();

                        lastLocation = location;
                        counter++;
                    }
                    for (int i = 0; i < speedBuckets.length; i++) {
                        if (speedBucketsCount[i] > 0) {
                            speedBuckets[i] /= speedBucketsCount[i];
                        }
                    }

                    trackResult.setDurationInSeconds((trackResult.getEndTime().getTime() - trackResult.getStartTime().getTime()) / 1000);
                    trackResult.setMaxSpeed(maxSpeed);
                    trackResult.setAvgSpeed(totalSpeed / locations.size());
                    trackResult.setAvgSpeeds(speedBuckets);
                    trackResult.setDistanceKM(totalDistanceMeters / 1000d);
                    trackResult.setTrackStatus(BMTrackResult.STATUS_EVALUATION_DONE);

                    trackResult.setPointsBlue(calculateBluePoints(trackResult));
                    trackResult.setPointsRed(calculateRedPoints(trackResult));

                    BasicAddress address = getLocation(locations.get(0));
                    if (address != null) {
                        trackResult.setStartAddressStreet(address.street);
                        trackResult.setStartAddressCity(address.city);
                    }
                    address = getLocation(locations.get(locations.size() - 1));
                    if (address != null) {
                        trackResult.setEndAddressStreet(address.street);
                        trackResult.setEndAddressCity(address.city);
                    }
                }
            }
            return trackResult;
        }

        private int calculateBluePoints(BMTrackResult trackResult) {
            return (int) ((App.sRng.nextFloat() + 0.5f) * 0.5f * trackResult.getDistanceKM() * 10);
        }

        private int calculateRedPoints(BMTrackResult trackResult) {
            return (int) ((App.sRng.nextFloat() + 0.0f) * 0.5f * trackResult.getDistanceKM() * 10);
        }

        private BasicAddress getLocation(BMLocation loc) {
            Geocoder geocoder = new Geocoder(getApplicationContext(), Locale.getDefault());
            // Create a list to contain the result address
            List<Address> addresses = null;
            try { // Return 1 address
                addresses = geocoder.getFromLocation(loc.getLatitude(), loc.getLongitude(), 1);
            } catch (IOException e1) {
                Log.e("LocationSampleActivity", "IO Exception in getFromLocation()");
            } catch (IllegalArgumentException e2) {
                String errorString = "Illegal arguments " + Double.toString(loc.getLatitude()) + " , " + Double.toString(loc.getLongitude()) + " passed to address service";
                Log.e("LocationSampleActivity", errorString);
            }
            // If the reverse geocode returned an address
            if (addresses != null && addresses.size() > 0) {
                // Get the first address
                Address address = addresses.get(0);
                // Format the first line of address (if available), city, and country name.
                BasicAddress basicAddress = new BasicAddress();

                // If there's a street address, add it
                basicAddress.street = address.getMaxAddressLineIndex() > 0 ? address.getAddressLine(0) : "";
                basicAddress.city = address.getLocality();

                return basicAddress;
            }
            return null;
        }

    }

    /**
     * Class used for the client Binder.  Because we know this service always
     * runs in the same process as its clients, we don't need to deal with IPC.
     */
    public class TrackingEvaluationServiceBinder extends Binder {
        public TrackingEvaluationService getService() {
            // Return this instance of TrackingService so clients can call public methods
            return TrackingEvaluationService.this;
        }
    }
}