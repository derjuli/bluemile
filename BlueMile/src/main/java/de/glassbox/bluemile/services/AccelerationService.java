package de.glassbox.bluemile.services;

import android.content.Intent;
import android.hardware.Sensor;
import android.hardware.SensorEvent;
import android.hardware.SensorEventListener;
import android.hardware.SensorManager;
import android.os.Binder;
import android.os.IBinder;
import android.util.Log;

import com.google.inject.Inject;

import de.glassbox.bluemile.model.BMTrackResult;
import de.glassbox.bluemile.persistence.DataAccess;
import roboguice.service.RoboService;

/**
 * Created by Juli on 17.05.14.
 */
public class AccelerationService extends RoboService implements SensorEventListener {

    // Update frequency in seconds
    private static final String TAG = AccelerationService.class.getSimpleName();
    // Data
    private static boolean sIsRunning = false;

    // Binder given to clients
    private final IBinder mBinder = new AccelerationServiceBinder();

    @Inject DataAccess mDataAccess;
    @Inject SensorManager mSensorManager;

    public static boolean isRunning() {
        return sIsRunning;
    }


    @Override
    public void onCreate() {
        super.onCreate();
        Log.i(TAG, "Service Started.");
        sIsRunning = true;

        // register this class as a listener for the orientation and accelerometer sensors
        mSensorManager.registerListener(this, mSensorManager.getDefaultSensor(Sensor.TYPE_ACCELEROMETER), SensorManager.SENSOR_DELAY_NORMAL);
    }

    @Override
    public IBinder onBind(Intent intent) {
        Log.d(TAG, "Binding service");
        return mBinder;
    }

    @Override
    public int onStartCommand(Intent intent, int flags, int startId) {
        Log.i(TAG, "Received start id " + startId + ": " + intent);

        return START_FLAG_REDELIVERY; // Run until explicitly stopped.
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        Log.i(TAG, "Service Stopped.");
        sIsRunning = false;
        mSensorManager.unregisterListener(this);
    }

    @Override
    public void onSensorChanged(SensorEvent sensorEvent) {
        Sensor mySensor = sensorEvent.sensor;

        if (mySensor.getType() == Sensor.TYPE_ACCELEROMETER) {
            float x = sensorEvent.values[0];
            float y = sensorEvent.values[1];
            float z = sensorEvent.values[2];

            long curTime = System.currentTimeMillis();

//            if ((curTime - lastUpdate) > 100) {
//                long diffTime = (curTime - lastUpdate);
//                lastUpdate = curTime;
//
//                float speed = Math.abs(x + y + z - last_x - last_y - last_z)/ diffTime * 10000;
//
//                if (speed > SHAKE_THRESHOLD) {
//
//                }
//
//                last_x = x;
//                last_y = y;
//                last_z = z;
//            }
        }
    }

    @Override
    public void onAccuracyChanged(Sensor sensor, int accuracy) {

    }

    /**
     * Creates a new tour for the given road and stores it in the database.
     */
    public void startTracking(BMTrackResult trackResult) {

        //mTimer.scheduleAtFixedRate(new LocationUpdateTask(), 500, sLocationUpdateTime);
    }

    public void stopTracking() {
        //mDataAccess.storeTrackResultAsync(mActiveTrackResult, null, false);
    }

    private void updateCurrentTour() {
        //mDataAccess.storeLocationAsync(loc, mActiveTrackResult.getLocalId());
    }


    /**
     * Class used for the client Binder.  Because we know this service always
     * runs in the same process as its clients, we don't need to deal with IPC.
     */
    public class AccelerationServiceBinder extends Binder {
        public AccelerationService getService() {
            // Return this instance of TrackingService so clients can call public methods
            return AccelerationService.this;
        }
    }
}
