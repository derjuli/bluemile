package de.glassbox.bluemile.services;

import android.content.Context;
import android.location.Criteria;
import android.location.Location;
import android.location.LocationManager;
import android.os.Build;
import android.util.Log;

import com.google.inject.Inject;
import com.google.inject.Singleton;

import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.NamedNodeMap;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;
import org.xml.sax.SAXException;

import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;

import de.glassbox.bluemile.App;
import de.glassbox.bluemile.R;


/**
 * Created by Juli on 12.02.14.
 */
@Singleton
public class MockLocationProvider {
    private static final String TAG = MockLocationProvider.class.getSimpleName();
    //private static final int[] sRoutes = new int[]{R.raw.bus50, R.raw.dachau_munich, R.raw.munich_poing};
    private static final int[] sRoutes = new int[]{R.raw.dachau_munich};
    private static final SimpleDateFormat sDateFormatter = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss"); // 2008-06-06T04:53:01Z
    private static final SimpleDateFormat sDateMSFormatter = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss.SSS"); // 2008-06-06T04:53:01.500Z

    private String mProviderName = LocationManager.GPS_PROVIDER;
    private double mCurrentLongitude;
    private double mCurrentLatitude;
    private LocationManager mLocationManager;

    private List<Location> mGPXList;
    private Context mContext;
    private int mCurrentNode;
    private Date mStartTimestamp;

    @Inject
    public MockLocationProvider(LocationManager locationManager, final Context context) {
        // Get GPS provider, or fallback if not enabled/existent
        Criteria criteria = new Criteria();
        criteria.setAccuracy(Criteria.ACCURACY_FINE);
        criteria.setPowerRequirement(Criteria.POWER_LOW);
        List<String> lProviders = locationManager.getProviders(true);
        for (int i = 0; i < lProviders.size(); i++) {
            if (lProviders.get(i) != null) {
                Log.d("LocationActivity", lProviders.get(i));
            }
        }
        String provider = locationManager.getBestProvider(criteria, false);

        this.mContext = context;
        this.mLocationManager = locationManager;

        if (provider != null && this.mLocationManager.isProviderEnabled(provider)) {
            this.mLocationManager.addTestProvider(provider, false, false, false, false, false, true, true, 0, 5);
            this.mLocationManager.setTestProviderEnabled(provider, true);
        }
    }

    public void loadRandomRoute() {
        mGPXList = null;
        mCurrentNode = 0;
        // load gpx tracking file
        Thread thread = new Thread(new Runnable() {
            @Override
            public void run() {
                int route = sRoutes[App.sRng.nextInt(sRoutes.length)];
                mGPXList = decodeGPX(mContext.getResources().openRawResource(route));
                mStartTimestamp = new Date(mGPXList.get(0).getTime());
                Log.d(TAG, "Finished Loading Track Data.");
            }
        });
        thread.start();
    }

    public void pushLocation() {
        mCurrentLatitude += 0.005d - App.sRng.nextDouble() / 100;
        mCurrentLongitude += 0.005d - App.sRng.nextDouble() / 100;
        pushLocation(mCurrentLatitude, mCurrentLongitude);
    }

    public void pushLocation(int timeDelta) {
        if (mGPXList != null && mCurrentNode < mGPXList.size()) {
            Location mockLocation = mGPXList.get(mCurrentNode);

            // check for new location to push. This makes sure to handle the time of the location correctly
            if (mStartTimestamp.getTime() >= mockLocation.getTime()) {
                Log.d(TAG, mockLocation.getLatitude() + " " + mockLocation.getLongitude());
                mockLocation.setProvider(mProviderName);
                mockLocation.setAltitude(0);
                mockLocation.setAccuracy(1);
                if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.JELLY_BEAN_MR1) {
                    mockLocation.setElapsedRealtimeNanos(System.nanoTime());
                }
                mLocationManager.setTestProviderLocation(mProviderName, mockLocation);
                mCurrentNode++;
            }
            mStartTimestamp.setTime(timeDelta + mStartTimestamp.getTime());
        }
    }

    public void pushLocation(double lat, double lon) {
        Log.d(TAG, mCurrentLatitude + " " + mCurrentLongitude);
        Location mockLocation = new Location(mProviderName);
        mockLocation.setLatitude(lat);
        mockLocation.setLongitude(lon);
        mockLocation.setAltitude(0);
        mockLocation.setAccuracy(1);
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.JELLY_BEAN_MR1) {
            mockLocation.setElapsedRealtimeNanos(System.nanoTime());
        }
        mockLocation.setTime(System.currentTimeMillis());
        mLocationManager.setTestProviderLocation(mProviderName, mockLocation);
    }

    public void shutdown() {
        mLocationManager.removeTestProvider(mProviderName);
    }

    private List<Location> decodeGPX(InputStream inputStream) {
        List<Location> list = new ArrayList<Location>();

        DocumentBuilderFactory documentBuilderFactory = DocumentBuilderFactory.newInstance();
        try {
            DocumentBuilder documentBuilder = documentBuilderFactory.newDocumentBuilder();
            //            FileInputStream fileInputStream = new FileInputStream(file);
            Document document = documentBuilder.parse(inputStream);
            Element elementRoot = document.getDocumentElement();

            NodeList nodelist_trkpt = elementRoot.getElementsByTagName("trkpt");

            for (int i = 0; i < nodelist_trkpt.getLength(); i++) {
                Node node = nodelist_trkpt.item(i);
                NamedNodeMap attributes = node.getAttributes();

                // parse location
                String newLatitude = attributes.getNamedItem("lat").getTextContent();
                Double newLatitude_double = Double.parseDouble(newLatitude);

                String newLongitude = attributes.getNamedItem("lon").getTextContent();
                Double newLongitude_double = Double.parseDouble(newLongitude);

                String newLocationName = newLatitude + ":" + newLongitude;

                // parse time
                NodeList nList = node.getChildNodes();
                Date date = new Date();
                for (int j = 0; j < nList.getLength(); j++) {
                    Node el = nList.item(j);
                    if (el.getNodeName().equals("time")) {
                        try {
                            String content = el.getTextContent();
                            if (content.contains(".")) {
                                date = sDateMSFormatter.parse(content);
                            } else {
                                date = sDateFormatter.parse(content);
                            }
                            break;
                        } catch (ParseException e) {
                            Log.e(TAG, org.apache.commons.lang3.exception.ExceptionUtils.getStackTrace(e));
                        }
                    }
                }

                // set the location
                Location newLocation = new Location(newLocationName);
                newLocation.setLatitude(newLatitude_double);
                newLocation.setLongitude(newLongitude_double);
                newLocation.setTime(date.getTime());

                list.add(newLocation);
            }

            inputStream.close();

        } catch (ParserConfigurationException e) {
            Log.e(TAG, org.apache.commons.lang3.exception.ExceptionUtils.getStackTrace(e));
        } catch (FileNotFoundException e) {
            Log.e(TAG, org.apache.commons.lang3.exception.ExceptionUtils.getStackTrace(e));
        } catch (SAXException e) {
            Log.e(TAG, org.apache.commons.lang3.exception.ExceptionUtils.getStackTrace(e));
        } catch (IOException e) {
            Log.e(TAG, org.apache.commons.lang3.exception.ExceptionUtils.getStackTrace(e));
        }

        return list;
    }
}