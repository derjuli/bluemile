package de.glassbox.bluemile.rest.requests;

import com.android.volley.AuthFailureError;
import com.android.volley.NetworkResponse;
import com.android.volley.ParseError;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.HttpHeaderParser;
import com.android.volley.toolbox.JsonRequest;

import org.json.JSONException;

import java.io.UnsupportedEncodingException;
import java.util.Map;

import de.glassbox.bluemile.model.User;
import de.glassbox.bluemile.persistence.ModelBuilder;
import de.glassbox.bluemile.utils.AuthUtils;

/**
 * Created by Juli on 19.06.2014.
 */
public class JsonClassRequest<T> extends JsonRequest {

    private User mUser;
    private Class mClazz;

    public JsonClassRequest(User user, int method, String url, String requestBody, Class clazz, Response.Listener listener, Response.ErrorListener errorListener) {
        super(method, url, requestBody, listener, errorListener);
        mUser = user;
        mClazz = clazz;
    }

    public JsonClassRequest(int method, String url, String requestBody, Class clazz, Response.Listener listener, Response.ErrorListener errorListener) {
        super(method, url, requestBody, listener, errorListener);
        mClazz = clazz;
    }

    @Override
    public Map<String, String> getHeaders() throws AuthFailureError {
        return AuthUtils.buildAuthHeaders(mUser.getEmail(), mUser.getPassword());
    }

    @Override
    protected Response<T> parseNetworkResponse(NetworkResponse response) {
        try {
            String responseString = new String(response.data, HttpHeaderParser.parseCharset(response.headers));
            return Response.success((T) ModelBuilder.createModelFromJSON(responseString, mClazz), HttpHeaderParser.parseCacheHeaders(response));
        } catch (UnsupportedEncodingException e) {
            return Response.error(new ParseError(e));
        } catch (JSONException ex) {
            return Response.error(new VolleyError(ex));
        }
    }
}
