package de.glassbox.bluemile.rest;

import android.content.Context;
import android.net.Uri;

import com.android.volley.AuthFailureError;
import com.android.volley.Request;
import com.android.volley.Response.ErrorListener;
import com.android.volley.Response.Listener;
import com.android.volley.toolbox.JsonRequest;
import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.google.inject.Inject;

import java.io.IOException;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import de.glassbox.bluemile.model.BMLocation;
import de.glassbox.bluemile.model.BMTrackResult;
import de.glassbox.bluemile.model.BMUserOverview;
import de.glassbox.bluemile.model.User;
import de.glassbox.bluemile.persistence.DataAccess;
import de.glassbox.bluemile.persistence.DataStore;
import de.glassbox.bluemile.persistence.ModelBuilder;
import de.glassbox.bluemile.rest.requests.JsonClassListRequest;
import de.glassbox.bluemile.rest.requests.JsonClassRequest;

public abstract class AbstractRestService {

    protected static final String NOT_IMPLEMENTED = "Not yet implemented";

    protected final DateFormat mDateFormat = new SimpleDateFormat("yyyy-MM-dd hh:mm:ss");
    protected final DateFormat mDateFormatMs = new SimpleDateFormat("yyyy-MM-dd hh:mm:ss,SS");

    @Inject
    protected Context mContext;
    @Inject
    protected RequestHandler mRequestHandler;
    @Inject
    protected DataAccess mDataAccess;
    @Inject
    protected DataStore mDataStore;

    public abstract void createAppUser(User user, Listener<User> listener, ErrorListener errorListener);

    public abstract void createAnonymousAppUser(User user, Listener<User> listener, ErrorListener errorListener);

    public abstract void upgradeAnonymousUser(User user, Listener<User> listener, ErrorListener errorListener);

    public abstract void updateAppUser(User user, Listener<User> listener, ErrorListener errorListener);

    public abstract void deleteAppUser(User user, Listener<Boolean> listener, ErrorListener errorListener);

    public abstract void resetUserPassword(String email, Listener<String> listener, ErrorListener errorListener);

    public abstract void loginUser(User user, Listener<User> listener, ErrorListener errorListener);

    public abstract void getMapData(long userId, long trackId, Listener<List<BMLocation>> listener, ErrorListener errorListener);

    public abstract void createTracking(User user, BMTrackResult trackResult, Listener<BMTrackResult> listener, ErrorListener errorListener);

    public abstract void getUserTracks(User user, Listener<List<BMTrackResult>> listener, ErrorListener errorListener);

    public abstract void getUserOverview(User user, Listener<BMUserOverview> listener, ErrorListener errorListener);

    protected Map<String, String> createParamMapping(Object object) {
        try {
            return new ObjectMapper().readValue(ModelBuilder.objectToJSON(object), new TypeReference<HashMap<String, String>>() {
            });
        } catch (IOException e) {
            e.printStackTrace();
        }
        return null;
    }

    protected String adjustUrl(String url, final Map<String, String> params) {
        // create url manually from params because volley doesn't support query params for get requests...
        Uri.Builder builder = new Uri.Builder();
        for (Map.Entry<String, String> entry : params.entrySet()) {
            builder.appendQueryParameter(entry.getKey(), entry.getValue());
        }
        return url + builder.build().toString();
    }

    protected <T> JsonRequest<T> createJsonListRequest(User user, String url, int method, final Map<String, String> params, Listener<List<T>> listener,
                                                       ErrorListener errorListener, final Class clazz) {
        url = adjustUrl(url, params);
        return new JsonClassListRequest<T>(user, method, url, null, clazz, listener, errorListener) {

        };
    }

    protected <T> Request<T> createJsonRequest(User user, String url, int method, final Map<String, String> params, Listener<T> listener, ErrorListener errorListener,
                                               Class clazz) {
        url = adjustUrl(url, params);

        return new JsonClassRequest<T>(user, method, url, null, clazz, listener, errorListener) {
            @Override
            protected Map<String, String> getParams() throws AuthFailureError {
                return params;
            }
        };
    }
}
