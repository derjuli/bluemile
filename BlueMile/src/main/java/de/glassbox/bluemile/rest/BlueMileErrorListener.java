package de.glassbox.bluemile.rest;

import com.android.volley.Response;
import com.android.volley.VolleyError;

import org.json.JSONException;

import de.glassbox.bluemile.persistence.ModelBuilder;


/**
 * Created by Juli on 06.06.2014.
 */
public abstract class BlueMileErrorListener implements Response.ErrorListener {

    protected abstract void onErrorResponse(BlueMileError error);

    @Override
    public void onErrorResponse(VolleyError error) {
        if (error.networkResponse != null && error.networkResponse.data != null) {
            String response = new String(error.networkResponse.data);

            try {
                onErrorResponse(ModelBuilder.createModelFromJSON(response, BlueMileError.class));
            } catch (JSONException e) {
                e.printStackTrace();
                onErrorResponse(new BlueMileError(error));
            }
        }
    }
}
