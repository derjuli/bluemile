package de.glassbox.bluemile.rest;

import android.content.Context;
import android.text.TextUtils;

import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.VolleyLog;
import com.android.volley.toolbox.HttpStack;
import com.android.volley.toolbox.HurlStack;
import com.android.volley.toolbox.Volley;
import com.google.inject.Inject;

import roboguice.inject.ContextSingleton;

/**
 * Helper class to easily access web services through get and post methods.
 *
 * @author Julian Sievers
 * @version 1.0
 */
@ContextSingleton
public class RequestHandler {

    private Context mContext;

    /**
     * Log or request TAG
     */
    public static final String TAG = "RequestHandler";

    /**
     * Global request queue for Volley
     */
    private RequestQueue mRequestQueue;

    private HttpStack mHurlStack;

    @Inject
    public RequestHandler(Context appContext) {
        this.mContext = appContext;
    }

    /**
     * @return The Volley Request queue, the queue will be created if it is null
     */
    public RequestQueue getRequestQueue() {
        if (mContext == null) {
            throw new IllegalStateException("Could not assign context.");
        }
        // lazy initialize the request queue, the queue instance will be
        // created when it is accessed for the first time
        if (mRequestQueue == null) {
            // Create an instance of a UrlConnection which automatically handles gzip.
            // We need this in order to access the cookie store
            mHurlStack = new HurlStack();
            // create the request queue
            mRequestQueue = Volley.newRequestQueue(mContext, mHurlStack);
        }

        return mRequestQueue;
    }

    /**
     * Adds the specified request to the global queue, if tag is specified then it is used else
     * Default TAG is used.
     *
     * @param req
     * @param tag
     */
    public <T> void addToRequestQueue(Request<T> req, String tag) {
        // set the default tag if tag is empty
        req.setTag(TextUtils.isEmpty(tag) ? TAG : tag);

        VolleyLog.d("Adding request to queue: %s", req.getUrl());

        getRequestQueue().add(req);
    }

    /**
     * Adds the specified request to the global queue using the Default TAG.
     *
     * @param req
     */
    public <T> void addToRequestQueue(Request<T> req) {
        // set the default tag if tag is empty
        req.setTag(TAG);

        getRequestQueue().add(req);
    }

    /**
     * Cancels all pending requests by the specified TAG, it is important to specify a TAG so that
     * the pending/ongoing requests can be cancelled.
     *
     * @param tag
     */
    public void cancelPendingRequests(Object tag) {
        if (mRequestQueue != null) {
            mRequestQueue.cancelAll(tag);
        }
    }
}
