package de.glassbox.bluemile.rest.requests;

import com.android.volley.AuthFailureError;
import com.android.volley.NetworkResponse;
import com.android.volley.ParseError;
import com.android.volley.Response;
import com.android.volley.toolbox.HttpHeaderParser;
import com.android.volley.toolbox.JsonRequest;

import java.io.UnsupportedEncodingException;
import java.util.List;
import java.util.Map;

import de.glassbox.bluemile.model.User;
import de.glassbox.bluemile.persistence.ModelBuilder;
import de.glassbox.bluemile.utils.AuthUtils;

/**
 * Created by Juli on 19.06.2014.
 */
public class JsonClassListRequest<T> extends JsonRequest {

    private User mUser;
    private Class mClazz;

    public JsonClassListRequest(User user, int method, String url, String requestBody, Class clazz, Response.Listener listener, Response.ErrorListener errorListener) {
        super(method, url, requestBody, listener, errorListener);
        mUser = user;
        mClazz = clazz;
    }

    public JsonClassListRequest(int method, String url, String requestBody, Class clazz, Response.Listener listener, Response.ErrorListener errorListener) {
        super(method, url, requestBody, listener, errorListener);
        mClazz = clazz;
    }

    @Override
    public Map<String, String> getHeaders() throws AuthFailureError {
        if (mUser != null) {
            return AuthUtils.buildAuthHeaders(mUser.getEmail(), mUser.getPassword());
        }
        return super.getHeaders();
    }

    @Override
    protected Response<List<T>> parseNetworkResponse(NetworkResponse networkResponse) {
        try {
            String responseString = new String(networkResponse.data, HttpHeaderParser.parseCharset(networkResponse.headers));
            return Response.success((List<T>) ModelBuilder.createListModelFromJSON(responseString, mClazz), HttpHeaderParser.parseCacheHeaders(networkResponse));
        } catch (UnsupportedEncodingException e) {
            return Response.error(new ParseError(e));
        }
    }
}
