package de.glassbox.bluemile.rest;

import com.android.volley.VolleyError;

/**
 * Created by Juli on 27.05.2014.
 */
public class BlueMileError {

    public static final String USER_NOT_FOUND = "U01";
    public static final String USER_ALREADY_EXISTS = "U02";

    private String errorCode;
    private String errorMsg;
    private VolleyError volleyError;

    public BlueMileError() {

    }

    public BlueMileError(VolleyError volleyError) {
        this.volleyError = volleyError;
    }

    public String getErrorCode() {
        return errorCode;
    }

    public String getErrorMsg() {
        return errorMsg;
    }

    public VolleyError getVolleyError() {
        return volleyError;
    }

    @Override
    public String toString() {
        String error = "error code: " + errorCode + "\nerrorMsg: " + errorMsg;
        if (volleyError != null) {
            error = "\nvolley message:" + volleyError.getMessage();
            if (volleyError.networkResponse != null) {
                error += "\nvolley response: " + new String(volleyError.networkResponse.data);
            }
        }
        return error;
    }

    public int getHttpCode() {
        if (volleyError != null && volleyError.networkResponse != null) {
            return volleyError.networkResponse.statusCode;
        }
        return -1;
    }
}
