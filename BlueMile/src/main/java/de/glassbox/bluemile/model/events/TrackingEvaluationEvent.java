package de.glassbox.bluemile.model.events;

import de.glassbox.bluemile.model.BMTrackResult;

/**
 * Created by Juli on 24.06.2014.
 */
public class TrackingEvaluationEvent {

    private BMTrackResult trackResult;

    public TrackingEvaluationEvent(BMTrackResult trackResult) {
        this.trackResult = trackResult;
    }

    public BMTrackResult getTrackResult() {
        return trackResult;
    }
}
