package de.glassbox.bluemile.model.events;

import de.glassbox.bluemile.model.BMLocation;

/**
 * Created by Juli on 09.06.2014.
 */
public class BMTrackingUpdateEvent {

        private int mSpeed;
        private double mTotalDistanceMeters;
        private double[] mGValues;
        private BMLocation mLocation;

        public BMTrackingUpdateEvent(int speed, double totalDistanceMeters, BMLocation location) {
                this.mSpeed = speed;
                this.mTotalDistanceMeters = totalDistanceMeters;
                this.mLocation = location;
        }

        public int getSpeed() {
                return mSpeed;
        }

        public void setSpeed(int speed) {
                this.mSpeed = speed;
        }

        public double getTotalDistanceMeters() {
                return mTotalDistanceMeters;
        }

        public void setTotalDistanceMeters(double totalDistanceMeters) {
                this.mTotalDistanceMeters = totalDistanceMeters;
        }

        public double[] getGValues() {
                return mGValues;
        }

        public void setGValues(double[] gValues) {
                this.mGValues = gValues;
        }

        public BMLocation getLocation() {
                return mLocation;
        }

        public void setLocation(BMLocation location) {
                this.mLocation = location;
        }
}
