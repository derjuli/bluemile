package de.glassbox.bluemile.model;

/**
 * Created by Juli on 19.05.2014.
 */
public class BMUserOverview {

    private double stars;
    private double km;
    private double miles;
    private double milesHighway;
    private double milesRural;
    private double milesUrban;
    private double milesAtNight;


    public double getStars() {
        return stars;
    }

    public void setStars(double stars) {
        this.stars = stars;
    }

    public double getKm() {
        return km;
    }

    public void setKm(double km) {
        this.km = km;
    }

    public double getMiles() {
        return miles;
    }

    public void setMiles(double miles) {
        this.miles = miles;
    }

    public double getMilesHighway() {
        return milesHighway;
    }

    public void setMilesHighway(double milesHighway) {
        this.milesHighway = milesHighway;
    }

    public double getMilesRural() {
        return milesRural;
    }

    public void setMilesRural(double milesRural) {
        this.milesRural = milesRural;
    }

    public double getMilesUrban() {
        return milesUrban;
    }

    public void setMilesUrban(double milesUrban) {
        this.milesUrban = milesUrban;
    }

    public double getMilesAtNight() {
        return milesAtNight;
    }

    public void setMilesAtNight(double milesAtNight) {
        this.milesAtNight = milesAtNight;
    }
}
