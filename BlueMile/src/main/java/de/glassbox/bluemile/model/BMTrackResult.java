package de.glassbox.bluemile.model;

import android.os.Parcel;
import android.os.Parcelable;

import com.fasterxml.jackson.annotation.JsonIgnore;

import org.apache.commons.lang3.StringUtils;

import java.util.Date;

/**
 * Created by Juli on 24.04.2014.
 */
public class BMTrackResult implements Parcelable {

    public static final String STATUS_CREATED = "CREATED";
    public static final String STATUS_EVALUATION_DONE = "EVALUATION_DONE";
    public static final Parcelable.Creator<BMTrackResult> CREATOR = new Parcelable.Creator<BMTrackResult>() {
        public BMTrackResult createFromParcel(Parcel source) {
            return new BMTrackResult(source);
        }

        public BMTrackResult[] newArray(int size) {
            return new BMTrackResult[size];
        }
    };
    @JsonIgnore
    private long localId;
    private long id;
    private double distanceKM = 0d;
    private int pointsRed;
    private int pointsBlue;
    private double kmUrban = 0d;
    private double kmRural = 0d;
    private double kmHighway = 0d;
    private double kmOther = 0d;
    private double kmDay = 0d;
    private double kmNight = 0d;
    private double durationInSeconds = 0d;
    private double avgSpeed = 0d;
    private double maxSpeed = 0d;
    private int[] avgSpeeds = new int[10];
    private String startAddress;
    private String startAddressStreet;
    private String startAddressCity;
    private String endAddress;
    private String endAddressStreet;
    private String endAddressCity;
    private Date startTime;
    private Date endTime;
    private String trackStatus;
    private double progress;

    /**
     * Empty ctor for jackson.
     */
    public BMTrackResult() {

    }

    private BMTrackResult(Parcel in) {
        this.localId = in.readLong();
        this.id = in.readLong();
        this.distanceKM = in.readDouble();
        this.pointsRed = in.readInt();
        this.pointsBlue = in.readInt();
        this.kmUrban = in.readDouble();
        this.kmRural = in.readDouble();
        this.kmHighway = in.readDouble();
        this.kmOther = in.readDouble();
        this.kmDay = in.readDouble();
        this.kmNight = in.readDouble();
        this.durationInSeconds = in.readDouble();
        this.avgSpeed = in.readDouble();
        this.maxSpeed = in.readDouble();
        this.avgSpeeds = in.createIntArray();
        this.startAddress = in.readString();
        this.startAddressStreet = in.readString();
        this.startAddressCity = in.readString();
        this.endAddress = in.readString();
        this.endAddressStreet = in.readString();
        this.endAddressCity = in.readString();
        long tmpStartTime = in.readLong();
        this.startTime = tmpStartTime == -1 ? null : new Date(tmpStartTime);
        long tmpEndTime = in.readLong();
        this.endTime = tmpEndTime == -1 ? null : new Date(tmpEndTime);
        this.trackStatus = in.readString();
        this.progress = in.readDouble();
    }

    public long getLocalId() {
        return localId;
    }

    public void setLocalId(long localId) {
        this.localId = localId;
    }

    public TrackStatus getTrackStatusEnum() {
        if (StringUtils.isEmpty(trackStatus)) {
            return TrackStatus.None;
        }
        if (StringUtils.equals(trackStatus, STATUS_CREATED)) {
            return TrackStatus.Created;
        }
        if (StringUtils.equals(trackStatus, STATUS_EVALUATION_DONE)) {
            return TrackStatus.Finished;
        }
        return TrackStatus.None;
    }

    public String getTrackStatus() {
        return trackStatus;
    }

    public void setTrackStatus(String trackStatus) {
        this.trackStatus = trackStatus;
    }

    public double getDistanceKM() {
        return distanceKM;
    }

    public void setDistanceKM(double distanceKM) {
        this.distanceKM = distanceKM;
    }

    public double getKmUrban() {
        return kmUrban;
    }

    public void setKmUrban(double kmUrban) {
        this.kmUrban = kmUrban;
    }

    public double getKmRural() {
        return kmRural;
    }

    public void setKmRural(double kmRural) {
        this.kmRural = kmRural;
    }

    public double getKmHighway() {
        return kmHighway;
    }

    public void setKmHighway(double kmHighway) {
        this.kmHighway = kmHighway;
    }

    public String getEndAddress() {
        return endAddress;
    }

    public void setEndAddress(String endAddress) {
        this.endAddress = endAddress;
    }

    public double getKmOther() {
        return kmOther;
    }

    public void setKmOther(double kmOther) {
        this.kmOther = kmOther;
    }

    public String getStartAddress() {
        return startAddress;
    }

    public void setStartAddress(String startAddress) {
        this.startAddress = startAddress;
    }

    public double getKmDay() {
        return kmDay;
    }

    public void setKmDay(double kmDay) {
        this.kmDay = kmDay;
    }

    public double getKmNight() {
        return kmNight;
    }

    public void setKmNight(double kmNight) {
        this.kmNight = kmNight;
    }

    public int[] getAvgSpeeds() {
        return avgSpeeds;
    }

    public void setAvgSpeeds(int[] avgSpeeds) {
        this.avgSpeeds = avgSpeeds;
    }

    public double getAvgSpeed() {
        return avgSpeed;
    }

    public void setAvgSpeed(double avgSpeed) {
        this.avgSpeed = avgSpeed;
    }

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public double getDurationInSeconds() {
        return durationInSeconds;
    }

    public void setDurationInSeconds(double durationInSeconds) {
        this.durationInSeconds = durationInSeconds;
    }

    public double getMaxSpeed() {
        return maxSpeed;
    }

    public void setMaxSpeed(double maxSpeed) {
        this.maxSpeed = maxSpeed;
    }

    public Date getStartTime() {
        if (startTime == null) {
            startTime = new Date();
        }
        return startTime;
    }

    public void setStartTime(Date startTime) {
        this.startTime = startTime;
    }

    public Date getEndTime() {
        if (endTime == null) {
            endTime = new Date();
        }
        return endTime;
    }

    public void setEndTime(Date endTime) {
        this.endTime = endTime;
    }

    public int getPointsRed() {
        return pointsRed;
    }

    public void setPointsRed(int pointsRed) {
        this.pointsRed = pointsRed;
    }

    public int getPointsBlue() {
        return pointsBlue;
    }

    public void setPointsBlue(int pointsBlue) {
        this.pointsBlue = pointsBlue;
    }

    public String getStartAddressStreet() {
        return startAddressStreet;
    }

    public void setStartAddressStreet(String startAddressStreet) {
        this.startAddressStreet = startAddressStreet;
    }

    public String getStartAddressCity() {
        return startAddressCity;
    }

    public void setStartAddressCity(String startAddressCity) {
        this.startAddressCity = startAddressCity;
    }

    public String getEndAddressStreet() {
        return endAddressStreet;
    }

    public void setEndAddressStreet(String endAddressStreet) {
        this.endAddressStreet = endAddressStreet;
    }

    public String getEndAddressCity() {
        return endAddressCity;
    }

    public void setEndAddressCity(String endAddressCity) {
        this.endAddressCity = endAddressCity;
    }

    public double getProgress() {
        return progress;
    }

    public void setProgress(double progress) {
        this.progress = progress;
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeLong(this.localId);
        dest.writeLong(this.id);
        dest.writeDouble(this.distanceKM);
        dest.writeInt(this.pointsRed);
        dest.writeInt(this.pointsBlue);
        dest.writeDouble(this.kmUrban);
        dest.writeDouble(this.kmRural);
        dest.writeDouble(this.kmHighway);
        dest.writeDouble(this.kmOther);
        dest.writeDouble(this.kmDay);
        dest.writeDouble(this.kmNight);
        dest.writeDouble(this.durationInSeconds);
        dest.writeDouble(this.avgSpeed);
        dest.writeDouble(this.maxSpeed);
        dest.writeIntArray(this.avgSpeeds);
        dest.writeString(this.startAddress);
        dest.writeString(this.startAddressStreet);
        dest.writeString(this.startAddressCity);
        dest.writeString(this.endAddress);
        dest.writeString(this.endAddressStreet);
        dest.writeString(this.endAddressCity);
        dest.writeLong(startTime != null ? startTime.getTime() : -1);
        dest.writeLong(endTime != null ? endTime.getTime() : -1);
        dest.writeString(this.trackStatus);
        dest.writeDouble(this.progress);
    }

    public enum TrackStatus {
        None, Created, Finished
    }
}
