package de.glassbox.bluemile.model.events;

/**
 * Created by Juli on 09.06.2014.
 */
public class TrackingEvent {

    private boolean trackingStartet;

    public TrackingEvent(boolean trackingStartet) {
        this.trackingStartet = trackingStartet;
    }

    public boolean isTrackingStartet() {
        return trackingStartet;
    }
}
