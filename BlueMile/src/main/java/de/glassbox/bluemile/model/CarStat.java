package de.glassbox.bluemile.model;

/**
 * Created by Juli on 22.06.2014.
 */
public class CarStat {

    public enum CarStatType {
        Acceleration, MaxSpeed, EcoFootprint
    }

    private CarStatType type;
    private int amount;

    public CarStatType getType() {
        return type;
    }

    public void setType(CarStatType name) {
        this.type = name;
    }

    public int getAmount() {
        return amount;
    }

    public void setAmount(int amount) {
        this.amount = amount;
    }
}
