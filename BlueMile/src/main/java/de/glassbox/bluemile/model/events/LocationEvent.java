package de.glassbox.bluemile.model.events;

import de.glassbox.bluemile.model.BMLocation;

/**
 * Created by Juli on 23.06.2014.
 */
public class LocationEvent {

        private BMLocation mLocation;

        public LocationEvent(BMLocation location) {
                mLocation = location;
        }

        public BMLocation getLocation() {
                return mLocation;
        }

        public void setLocation(BMLocation mLocation) {
                this.mLocation = mLocation;
        }
}
