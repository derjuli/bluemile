package de.glassbox.bluemile.model.events;

import de.glassbox.bluemile.model.BMTrackResult;

/**
 * Created by Juli on 09.06.2014.
 */
public class BMTrackResultUploadEvent {

    private BMTrackResult trackResult;
    private boolean couldNotFinish;

    public BMTrackResultUploadEvent(boolean couldNotFinish) {
        this.couldNotFinish = couldNotFinish;
    }

    public BMTrackResultUploadEvent(BMTrackResult trackResult) {
        this.trackResult = trackResult;
    }

    public BMTrackResult getTrackResult() {
        return trackResult;
    }

    public boolean couldNotFinish() {
        return couldNotFinish;
    }
}
