package de.glassbox.bluemile.model;

import java.util.List;

/**
 * Created by Juli on 22.06.2014.
 */
public class CarPart {

    private String name;
    private String description;
    private List<CarStat> carStats;
    private int costRed;
    private int costBlue;
    private boolean owned;
    private boolean installed;

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public List<CarStat> getCarStats() {
        return carStats;
    }

    public void setCarStats(List<CarStat> carStats) {
        this.carStats = carStats;
    }

    public int getCostRed() {
        return costRed;
    }

    public void setCostRed(int costRed) {
        this.costRed = costRed;
    }

    public int getCostBlue() {
        return costBlue;
    }

    public void setCostBlue(int costBlue) {
        this.costBlue = costBlue;
    }

    public boolean isOwned() {
        return owned;
    }

    public void setOwned(boolean owned) {
        this.owned = owned;
    }

    public boolean isInstalled() {
        return installed;
    }

    public void setInstalled(boolean installed) {
        this.installed = installed;
    }
}
