package de.glassbox.bluemile.model;

import com.google.android.gms.games.leaderboard.LeaderboardScore;

/**
 * Created by Juli on 06.07.2014.
 */
public class LeaderboardItem {

    private String image;
    private String username;
    private String displayScore;
    private long score;
    private long rank;

    public static LeaderboardItem fromLeaderboardScore(LeaderboardScore score) {
        LeaderboardItem item = new LeaderboardItem();
        item.displayScore = score.getDisplayScore();
        item.score = score.getRawScore();
        item.image = score.getScoreHolderIconImageUrl();
        item.rank = score.getRank();
        item.username = score.getScoreHolderDisplayName();
        return item;
    }

    public String getImage() {
        return image;
    }

    public String getUsername() {
        return username;
    }

    public String getDisplayScore() {
        return displayScore;
    }

    public long getScore() {
        return score;
    }

    public long getRank() {
        return rank;
    }
}
