package de.glassbox.bluemile.model;

/**
 * Created by Juli on 19.06.2014.
 */
public interface User {

    String getFirstname();

    void setFirstname(String firstname);

    String getLastname();

    void setLastname(String lastname);

    String getGender();

    void setGender(String gender);

    String getBirthday();

    void setBirthday(String birthday);

    long getUserId();

    void setUserId(long userId);

    String getEmail();

    void setEmail(String email);

    String getPassword();

    void setPassword(String password);

    boolean isAnonymous();

    void setAnonymous(boolean anonymous);
}
