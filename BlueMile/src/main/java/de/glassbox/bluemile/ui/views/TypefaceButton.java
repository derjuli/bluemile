package de.glassbox.bluemile.ui.views;

import android.content.Context;
import android.content.res.TypedArray;
import android.graphics.Typeface;
import android.text.Html;
import android.util.AttributeSet;
import android.view.Gravity;
import android.widget.Button;

import org.apache.commons.lang3.StringUtils;

import de.glassbox.bluemile.R;
import de.glassbox.bluemile.utils.Typefaces;


/**
 * Improved {@link android.widget.TextView} that can have a custom font applied.
 *
 * @author Julian Sievers
 * @version 1.0
 */
public class TypefaceButton extends Button {
    private boolean mRichText;

    public TypefaceButton(final Context context) {
        this(context, null);
    }

    public TypefaceButton(final Context context, final AttributeSet attrs) {
        this(context, attrs, 0);
    }

    public TypefaceButton(final Context context, final AttributeSet attrs, final int defStyle) {
        super(context, attrs, defStyle);

        final TypedArray a = context.obtainStyledAttributes(attrs, R.styleable.TypefaceTextView);
        final String font = a.getString(R.styleable.TypefaceTextView_customFont);

        if (!isInEditMode()) {
            setCustomFont(context, font);
        }

        mRichText = a.getBoolean(R.styleable.TypefaceTextView_richText, false);
        if (a.getBoolean(R.styleable.TypefaceTextView_onlyXmlText, false)) {
            setText(getText());
        }

        setGravity(Gravity.CENTER);
        int padding = (int) getResources().getDimension(R.dimen.button_padding);
        setPadding(padding, padding, padding, padding);

        a.recycle();
    }

    @Override
    public void setText(CharSequence text, BufferType type) {
        super.setText(mRichText ? Html.fromHtml(text.toString()) : text, type);
    }

    /**
     * Loads a given font from the assets folder and applies the font to the TextView.
     *
     * @param context   The current context.
     * @param fontAsset The font asset to load.
     */
    public void setCustomFont(final Context context, String fontAsset) {
        if (StringUtils.isEmpty(fontAsset)) {
            return;
        }

        Typeface tf = Typefaces.get(context, fontAsset);
        if (tf == null) {
            return;
            //            fontAsset = getResources().getString(R.string.font_standard);
            //            tf = Typefaces.get(context, fontAsset);
        }

        setTypeface(tf);
    }

}