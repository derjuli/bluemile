package de.glassbox.bluemile.ui.adapters;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TextView;

import com.google.android.gms.games.leaderboard.Leaderboard;

import java.util.List;

import de.glassbox.bluemile.R;

/**
 * Created by Juli on 21.06.2014.
 */
public class LeaderboardSelectionAdapter extends ArrayAdapter<Leaderboard> {

    public LeaderboardSelectionAdapter(Context context) {
        super(context, 0);
    }

    public LeaderboardSelectionAdapter(Context context, List<Leaderboard> leaderboards) {
        super(context, 0, leaderboards);
    }

    public View getView(int position, View convertView, ViewGroup parent) {
        return createView(position, convertView);
    }

    @Override
    public View getDropDownView(int position, View convertView, ViewGroup parent) {
        return createView(position, convertView);
    }

    private View createView(int position, View convertView) {
        if (convertView == null) {
            // TODO use viewholder
            convertView = LayoutInflater.from(getContext()).inflate(R.layout.leaderboard_spinner_item, null);
        }

        Leaderboard item = getItem(position);
        TextView title = (TextView) convertView.findViewById(R.id.title);
        title.setText(item.getDisplayName());

//        ImageView imageView = (ImageView) convertView.findViewById(R.id.sidebar_item_icon);
//        imageView.setImageResource(item.iconRes);
//        imageView.setBackgroundColor(item.iconColorRes);
        return convertView;
    }

}
