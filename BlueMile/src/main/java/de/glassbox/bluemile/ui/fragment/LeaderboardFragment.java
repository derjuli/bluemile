package de.glassbox.bluemile.ui.fragment;


import android.app.Fragment;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ListView;
import android.widget.ProgressBar;
import android.widget.TextView;

import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.common.api.PendingResult;
import com.google.android.gms.common.api.ResultCallback;
import com.google.android.gms.games.Games;
import com.google.android.gms.games.leaderboard.LeaderboardScore;
import com.google.android.gms.games.leaderboard.LeaderboardScoreBuffer;
import com.google.android.gms.games.leaderboard.LeaderboardVariant;
import com.google.android.gms.games.leaderboard.Leaderboards;

import de.glassbox.bluemile.R;
import de.glassbox.bluemile.model.LeaderboardItem;
import de.glassbox.bluemile.ui.adapters.LeaderboardAdapter;
import de.glassbox.bluemile.ui.callback.GoogleApiProvider;
import roboguice.fragment.RoboFragment;
import roboguice.inject.InjectView;

/**
 * A simple {@link Fragment} subclass.
 * Use the {@link LeaderboardFragment#newInstance} factory method to
 * create an instance of this fragment.
 */
public class LeaderboardFragment extends RoboFragment {

    String mLeaderboardId;
    int mSpan;
    int mLimit;
    @InjectView(R.id.score_list) ListView mScoreList;
    @InjectView(R.id.score_title) TextView mNumPlayers;
    @InjectView(R.id.progressBar) ProgressBar mProgress;
    PendingResult<Leaderboards.LoadScoresResult> mLoadScoresResultPendingResult;

    public LeaderboardFragment() {
    }

    // TODO: Rename and change types and number of parameters
    public static LeaderboardFragment newInstance(String leaderboardId, int span, int limit) {
        LeaderboardFragment fragment = new LeaderboardFragment();
        Bundle args = new Bundle();
        args.putString("leaderboardId", leaderboardId);
        args.putInt("span", span);
        args.putInt("limit", limit);
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
            mLeaderboardId = getArguments().getString("leaderboardId");
            mSpan = getArguments().getInt("span");
            mLimit = getArguments().getInt("limit");
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_leaderboard, container, false);
    }

    @Override
    public void onViewCreated(View view, Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        mProgress.setVisibility(View.VISIBLE);
        GoogleApiClient client = ((GoogleApiProvider) getActivity()).getGoogleApiClient();
        mLoadScoresResultPendingResult = Games.Leaderboards.loadTopScores(client, mLeaderboardId, mSpan, LeaderboardVariant.COLLECTION_PUBLIC, mLimit);
        mLoadScoresResultPendingResult.setResultCallback(
            new ResultCallback<Leaderboards.LoadScoresResult>() {
                @Override
                public void onResult(Leaderboards.LoadScoresResult loadScoresResult) {
                    if (isAdded() && !isDetached()) {
                        String[] playerString = getResources().getStringArray(R.array.numberOfPlayers);
                        int n;
                        int numPlayers = Math.max(0, (int) loadScoresResult.getLeaderboard().getVariants().get(0).getNumScores());
                        if (numPlayers > 100) {
                            numPlayers = numPlayers - numPlayers % 100;
                            n = 2;
                        } else {
                            n = numPlayers == 1 ? 0 : 1;
                        }
                        mNumPlayers.setText(String.format(playerString[n], numPlayers));
                        createLeaderboardFromScores(loadScoresResult.getScores());
                        mProgress.setVisibility(View.GONE);
                    }
                }
            }
        );
    }

    @Override
    public void onStop() {
        super.onStop();
        if (mLoadScoresResultPendingResult != null) {
            mLoadScoresResultPendingResult.cancel();
        }
    }

    private void createLeaderboardFromScores(LeaderboardScoreBuffer leaderboardScores) {
        LeaderboardAdapter leaderboardAdapter = new LeaderboardAdapter(getActivity());
        for (LeaderboardScore leaderboardScore : leaderboardScores) {
            // TODO duplicated score for mocking
            LeaderboardItem item = LeaderboardItem.fromLeaderboardScore(leaderboardScore);
            leaderboardAdapter.add(item);
            leaderboardAdapter.add(item);
            leaderboardAdapter.add(item);
            leaderboardAdapter.add(item);
            leaderboardAdapter.add(item);
        }
        leaderboardScores.close();
        leaderboardAdapter.notifyDataSetChanged();
        mScoreList.setAdapter(leaderboardAdapter);
    }
}
