package de.glassbox.bluemile.ui.fragment;


import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;
import android.support.v4.app.FragmentTransaction;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.google.inject.Inject;
import com.squareup.otto.Bus;
import com.squareup.otto.Subscribe;
import com.viewpagerindicator.UnderlinePageIndicator;

import de.glassbox.bluemile.R;
import de.glassbox.bluemile.model.events.TrackingEvent;
import de.glassbox.bluemile.ui.callback.ChangeDrawerTitle;
import de.glassbox.bluemile.ui.views.LockableViewPager;
import roboguice.inject.InjectView;

/**
 * A simple {@link Fragment} subclass.
 * Use the {@link StartFragment#newInstance} factory method to
 * create an instance of this fragment.
 */
public class StartFragment extends NavigationDrawerBaseFragment {

    @InjectView(R.id.pager) LockableViewPager mPager;
    @InjectView(R.id.indicator) UnderlinePageIndicator mPagerIndicator;
    @Inject Bus mEventBus;

    public StartFragment() {
    }

    public static NavigationDrawerBaseFragment newInstance(int section) {
        NavigationDrawerBaseFragment fragment = new StartFragment();
        fragment.setArguments(section);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
        }
    }

    @Override
    public void onResume() {
        super.onResume();
        mEventBus.register(this);
    }

    @Override
    public void onPause() {
        super.onPause();
        mEventBus.unregister(this);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        if (savedInstanceState == null) {
            FragmentTransaction transaction = getChildFragmentManager().beginTransaction();
            transaction.add(R.id.tracking_fragment, TrackingFragment.newInstance(0));
            transaction.commit();
        }

        return inflater.inflate(R.layout.fragment_start, container, false);
    }

    @Override
    public void onViewCreated(View view, Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        FragmentPagerAdapter adapter = new MainPagerAdapter(getChildFragmentManager());
        mPager.setAdapter(adapter);
        mPagerIndicator.setViewPager(mPager);
    }

    @Subscribe
    public void onTrackingEvent(TrackingEvent trackingEvent) {
        if (getActivity() instanceof ChangeDrawerTitle) {
            ((ChangeDrawerTitle) getActivity()).restoreActionBar();
        }

        if (trackingEvent.isTrackingStartet()) {
            mPager.setCurrentItem(1, true);
            mPager.setPagingEnabled(false);
            mPagerIndicator.setFades(true);
        } else {
            mPager.setPagingEnabled(true);
            mPagerIndicator.setFades(false);
        }
    }

    static class MainPagerAdapter extends FragmentPagerAdapter {
        public MainPagerAdapter(FragmentManager fm) {
            super(fm);
        }

        @Override
        public Fragment getItem(int position) {
            switch (position) {
                case 0:
                    return CarFragment.newInstance();
                case 1:
                    return TrackingMapFragment.newInstance();
            }
            return null;
        }

        @Override
        public int getCount() {
            return 2;
        }
    }
}
