package de.glassbox.bluemile.ui.fragment;


import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseExpandableListAdapter;
import android.widget.ExpandableListView;
import android.widget.ImageView;
import android.widget.TextView;

import com.google.inject.Inject;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.HashMap;
import java.util.List;

import de.glassbox.bluemile.R;
import de.glassbox.bluemile.model.BMTrackResult;
import de.glassbox.bluemile.persistence.DataAccess;
import de.glassbox.bluemile.persistence.DataStore;
import de.glassbox.bluemile.ui.activities.HistoryDetailActivity;
import de.glassbox.bluemile.utils.DateUtils;

public class HistoryFragment extends NavigationDrawerBaseFragment implements ExpandableListView.OnChildClickListener {

    // Create an instance of SimpleDateFormat used for formatting
    DateFormat headerDateFormat = new SimpleDateFormat("MMMM yyyy");
    DateFormat contentDateFormat = new SimpleDateFormat("dd.MM.yyyy");
    // Views
    private ExpandableListView mFragmentView;
    private ExpandableTrackListAdapter mAdapter;
    @Inject
    private DataStore mDataStore;
    @Inject
    private DataAccess mDataAccess;

    public HistoryFragment() {
    }

    public static NavigationDrawerBaseFragment newInstance(int section) {
        NavigationDrawerBaseFragment fragment = new HistoryFragment();
        return fragment.setArguments(section);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        if (mFragmentView == null) {
            mFragmentView = (ExpandableListView) inflater.inflate(R.layout.fragment_history, null, false);
            mFragmentView.setOnChildClickListener(this);
        } else {
            ((ViewGroup) mFragmentView.getParent()).removeView(mFragmentView);
        }

        return mFragmentView;
    }

    private void loadData() {
        mDataAccess.getTrackResultsAsync(new DataAccess.DeliverResult<List<BMTrackResult>>() {
            @Override
            public void onResult(List<BMTrackResult> data) {
                if (data != null && !isDetached()) {
                    mAdapter = new ExpandableTrackListAdapter(getActivity());
                    mAdapter.setData(data);
                    mFragmentView.setAdapter(mAdapter);
                }
            }
        }, true);
    }

    @Override
    public void onViewCreated(View view, Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        loadData();
    }

    @Override
    public boolean onChildClick(ExpandableListView parent, View v, int groupPosition, int childPosition, long id) {
        BMTrackResult track = (BMTrackResult) parent.getExpandableListAdapter().getChild(groupPosition, childPosition);

        Intent intent = new Intent(getActivity(), HistoryDetailActivity.class);
        intent.putExtra(HistoryDetailActivity.EXTRA_TRACK_RESULT, track);
        startActivity(intent);

        // ((BaseContainerFragment) getParentFragment()).replaceFragment(new TourDetailsFragment(), true);
        //((ExchangableFragmentContainer) getActivity()).replaceFragment(new TourDetailsFragment(), true);

        return true;
    }

    private static class HeaderViewHolder {
        TextView mDateView;
        TextView mTourCountView;
        TextView mMilesView;
        TextView mPointsBlueView;
        TextView mPointsRedView;
    }

    private static class ContentViewHolder {
        ImageView mRoadIconView;
        TextView mDateView;
        TextView mMilesView;
        TextView mPointsBlueView;
        TextView mPointsRedView;
        TextView mTourTimeView;
    }

    private static class GroupDataWrapper {
        String mDate;
        int mItemCount;
        int mTotalDistance;
        int mPointsBlue;
        int mPointsRed;
    }

    public class ExpandableTrackListAdapter extends BaseExpandableListAdapter {

        private LayoutInflater mInflater;
        private List<GroupDataWrapper> mListDataHeader = new ArrayList<GroupDataWrapper>(); // group data
        private HashMap<GroupDataWrapper, List<BMTrackResult>> mListDataChild = new HashMap<GroupDataWrapper, List<BMTrackResult>>();  // groups

        public ExpandableTrackListAdapter(Context context) {
            this.mInflater = LayoutInflater.from(context);
        }

        public void setData(List<BMTrackResult> trackResults) {
            splitTracksByMonth(trackResults);
        }

        private void splitTracksByMonth(List<BMTrackResult> tracks) {
            int year = 0;
            int month = 0;
            mListDataHeader.clear();
            mListDataChild.clear();
            List<BMTrackResult> currentGroup = new ArrayList<BMTrackResult>();

            for (int i = 0; i < tracks.size(); i++) {
                BMTrackResult track = tracks.get(i);

                Calendar cal = Calendar.getInstance();
                cal.setTime(track.getStartTime());
                int tYear = cal.get(Calendar.YEAR);
                int tMonth = cal.get(Calendar.MONTH);

                if (year != tYear || month != tMonth) {
                    // not in same month, create new group
                    createNewHeaderGroup(currentGroup);
                    currentGroup = new ArrayList<BMTrackResult>();

                    year = tYear;
                    month = tMonth;
                }
                currentGroup.add(track);
            }
            createNewHeaderGroup(currentGroup);
        }

        private void createNewHeaderGroup(List<BMTrackResult> tracks) {
            if (tracks != null && tracks.size() > 0) {
                GroupDataWrapper group = new GroupDataWrapper();
                group.mItemCount = tracks.size();
                group.mDate = headerDateFormat.format(tracks.get(0).getStartTime()).toUpperCase();

                for (BMTrackResult track : tracks) {
                    group.mPointsBlue += track.getPointsBlue();
                    group.mPointsRed += track.getPointsRed();
                    group.mTotalDistance += track.getDistanceKM();
                }

                mListDataHeader.add(group);
                mListDataChild.put(group, tracks);
            }
        }

        @Override
        public Object getChild(int groupPosition, int childPosititon) {
            return this.mListDataChild.get(this.mListDataHeader.get(groupPosition)).get(childPosititon);
        }

        @Override
        public long getChildId(int groupPosition, int childPosition) {
            return childPosition;
        }

        @Override
        public View getChildView(int groupPosition, final int childPosition, boolean isLastChild, View convertView, ViewGroup parent) {
            // create or recycle view
            ContentViewHolder cvh;
            if (convertView == null) {
                convertView = mInflater.inflate(R.layout.cockpit_tour_list_item, null);

                cvh = new ContentViewHolder();
                cvh.mRoadIconView = (ImageView) convertView.findViewById(R.id.road_icon);
                cvh.mDateView = (TextView) convertView.findViewById(R.id.tour_date);
                cvh.mMilesView = (TextView) convertView.findViewById(R.id.distance_overall);
                cvh.mTourTimeView = (TextView) convertView.findViewById(R.id.tour_time);
                cvh.mPointsBlueView = (TextView) convertView.findViewById(R.id.points_blue);
                cvh.mPointsRedView = (TextView) convertView.findViewById(R.id.points_red);

                convertView.setTag(R.layout.cockpit_tour_list_item, cvh);
            } else {
                cvh = (ContentViewHolder) convertView.getTag(R.layout.cockpit_tour_list_item);
            }

            // create normal item
            BMTrackResult track = (BMTrackResult) getChild(groupPosition, childPosition);

            cvh.mDateView.setText(contentDateFormat.format(track.getStartTime()));
            cvh.mMilesView.setText(String.format(getString(R.string.history_total_distance), (int) track.getDistanceKM()));

            cvh.mTourTimeView.setText(DateUtils.formatElapsedTime((track.getEndTime().getTime() - track.getStartTime().getTime()) / 1000, true));
            cvh.mPointsBlueView.setText(Integer.toString(track.getPointsBlue()));
            cvh.mPointsRedView.setText(Integer.toString(track.getPointsRed()));

            return convertView;
        }

        @Override
        public int getChildrenCount(int groupPosition) {
            return this.mListDataChild.get(this.mListDataHeader.get(groupPosition)).size();
        }

        @Override
        public Object getGroup(int groupPosition) {
            return this.mListDataHeader.get(groupPosition);
        }

        @Override
        public int getGroupCount() {
            return this.mListDataHeader.size();
        }

        @Override
        public long getGroupId(int groupPosition) {
            return groupPosition;
        }

        @Override
        public View getGroupView(int groupPosition, boolean isExpanded, View convertView, ViewGroup parent) {
            // create or recycle view
            HeaderViewHolder hvh;
            if (convertView == null) {
                convertView = mInflater.inflate(R.layout.cockpit_tour_list_header, null);

                hvh = new HeaderViewHolder();
                hvh.mDateView = (TextView) convertView.findViewById(R.id.track_date);
                hvh.mMilesView = (TextView) convertView.findViewById(R.id.track_distance);
                hvh.mPointsBlueView = (TextView) convertView.findViewById(R.id.track_points_blue);
                hvh.mPointsRedView = (TextView) convertView.findViewById(R.id.track_points_red);
                hvh.mTourCountView = (TextView) convertView.findViewById(R.id.track_count);

                convertView.setTag(R.layout.cockpit_tour_list_header, hvh);
            } else {
                hvh = (HeaderViewHolder) convertView.getTag(R.layout.cockpit_tour_list_header);
            }

            GroupDataWrapper headerTitle = (GroupDataWrapper) getGroup(groupPosition);
            hvh.mDateView.setText(headerTitle.mDate);
            hvh.mTourCountView.setText(String.format(getString(R.string.history_tour_count), headerTitle.mItemCount));
            hvh.mMilesView.setText(String.format(getString(R.string.history_total_distance), headerTitle.mTotalDistance));
            hvh.mPointsBlueView.setText(Integer.toString(headerTitle.mPointsBlue));
            hvh.mPointsRedView.setText(Integer.toString(headerTitle.mPointsRed));

            return convertView;
        }

        @Override
        public boolean hasStableIds() {
            return false;
        }

        @Override
        public boolean isChildSelectable(int groupPosition, int childPosition) {
            return true;
        }
    }
}
