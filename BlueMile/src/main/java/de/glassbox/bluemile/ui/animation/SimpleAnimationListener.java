package de.glassbox.bluemile.ui.animation;

import android.view.animation.Animation;

/**
 * Created by Juli on 20.06.2014.
 */
public class SimpleAnimationListener implements Animation.AnimationListener {
    @Override
    public void onAnimationStart(Animation animation) {

    }

    @Override
    public void onAnimationEnd(Animation animation) {

    }

    @Override
    public void onAnimationRepeat(Animation animation) {

    }
}
