package de.glassbox.bluemile.ui.fragment.dialog;

import android.app.Activity;
import android.content.DialogInterface;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import org.apache.commons.lang3.StringUtils;

import de.glassbox.bluemile.R;
import roboguice.inject.InjectView;

/**
 * Created by Juli on 22.04.2014.
 */
public class ErrorDialogFragment extends StyleableDialogFragment implements View.OnClickListener {

    /* Data */

    /* Views */
    @InjectView(R.id.popup_header)
    private TextView mHeadline;
    @InjectView(R.id.popup_error_description)
    private TextView mDescription;
    @InjectView(R.id.popup_error_ok)
    private View mOkButton;
    @InjectView(R.id.popup_close)
    private View mCloseButton;

    /**
     * Create a new instance of MyDialogFragment, providing "num"
     * as an argument.
     */
    public static ErrorDialogFragment newInstance(String headline, String description) {
        ErrorDialogFragment f = new ErrorDialogFragment();

        // Supply num input as an argument.
        Bundle args = new Bundle();
        args.putString("headline", headline);
        args.putString("description", description);
        f.setArguments(args);

        return f;
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        return onCreateView(inflater, container, R.layout.popup_error, savedInstanceState);
    }

    @Override
    public void onDismiss(DialogInterface dialog) {
        super.onDismiss(dialog);
        Fragment target = getTargetFragment();
        if (target != null) {
            target.onActivityResult(0, Activity.RESULT_OK, null);
        }
    }

    @Override
    public void onViewCreated(View view, Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        if (StringUtils.isNotEmpty(getArguments().getString("headline"))) {
            mHeadline.setText(getArguments().getString("headline"));
        }
        mDescription.setText(getArguments().getString("description"));
        mOkButton.setOnClickListener(this);
        mCloseButton.setOnClickListener(this);
    }

    @Override
    public void onClick(View v) {
        this.dismiss();
    }
}
