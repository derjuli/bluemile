package de.glassbox.bluemile.ui.adapters;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import com.nostra13.universalimageloader.core.ImageLoader;

import java.util.List;

import de.glassbox.bluemile.R;
import de.glassbox.bluemile.model.LeaderboardItem;
import de.glassbox.bluemile.utils.ImageLoaderUtils;
import de.glassbox.bluemile.utils.StringUtils;

/**
 * Created by Juli on 01.07.2014.
 */
public class LeaderboardAdapter extends ArrayAdapter<LeaderboardItem> {

    private static final String TAG = LeaderboardAdapter.class.getSimpleName();

    private static final int ITEMTYPE_NORMAL = 0;
    private static final int ITEMTYPE_LARGE = 1;

    public LeaderboardAdapter(Context context) {
        super(context, 0);
    }

    public LeaderboardAdapter(Context context, List<LeaderboardItem> scores) {
        super(context, 0, scores);
    }

    @Override
    public int getViewTypeCount() {
        return 2;
    }

    @Override
    public int getItemViewType(int position) {
        return ITEMTYPE_NORMAL;
    }

    public View getView(int position, View convertView, ViewGroup parent) {
        if (getItemViewType(position) == ITEMTYPE_NORMAL) {
            return createNormalView(position, convertView);
        }

        return createLargeView(position, convertView);
    }

    private View createLargeView(int position, View convertView) {
        return null;
    }

    private View createNormalView(int position, View convertView) {
        NormalScoreViewHolder viewHolder;
        if (convertView == null) {
            convertView = LayoutInflater.from(getContext()).inflate(R.layout.listitem_score_normal, null);

            viewHolder = new NormalScoreViewHolder();
            viewHolder.image = (ImageView) convertView.findViewById(R.id.score_image);
            viewHolder.username = (TextView) convertView.findViewById(R.id.score_name);
            viewHolder.score = (TextView) convertView.findViewById(R.id.score_points);
            viewHolder.place = (TextView) convertView.findViewById(R.id.score_place);
            convertView.setTag(viewHolder);
        } else {
            viewHolder = (NormalScoreViewHolder) convertView.getTag();
        }

        LeaderboardItem item = getItem(position);

        if (item.getImage() != null) {
            ImageLoader.getInstance().displayImage(item.getImage(), viewHolder.image, ImageLoaderUtils.getOnDiscOptions());
        }
        viewHolder.username.setText(item.getUsername());
        viewHolder.score.setText(item.getDisplayScore());
        viewHolder.place.setText(item.getRank() + StringUtils.getNumberSuffix(item.getRank()));

        return convertView;
    }

    private static class NormalScoreViewHolder {
        ImageView image;
        TextView username;
        TextView score;
        TextView place;
    }
}
