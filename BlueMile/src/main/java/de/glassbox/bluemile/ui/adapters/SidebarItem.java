package de.glassbox.bluemile.ui.adapters;

/**
 * Created by Juli on 21.06.2014.
 */
public class SidebarItem {
    public String text;
    public int iconRes;
    public int iconColorRes;

    public SidebarItem(String text, int iconRes, int iconColorRes) {
        this.text = text;
        this.iconRes = iconRes;
        this.iconColorRes = iconColorRes;
    }
}