package de.glassbox.bluemile.ui.callback;

import android.support.v4.view.ViewPager;

/**
 * Created by Juli on 20.06.2014.
 */
public class ViewPagerListenerImpl implements ViewPagerListener {

    private ViewPager mViewPager;

    public ViewPagerListenerImpl(ViewPager viewPager) {
        mViewPager = viewPager;
    }

    @Override
    public void nextPage(boolean smoothScroll) {
        mViewPager.setCurrentItem(mViewPager.getCurrentItem() + 1, smoothScroll);
    }

    @Override
    public void previousPage(boolean smoothScroll) {
        mViewPager.setCurrentItem(mViewPager.getCurrentItem() - 1, smoothScroll);
    }
}
