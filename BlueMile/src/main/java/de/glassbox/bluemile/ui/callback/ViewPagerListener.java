package de.glassbox.bluemile.ui.callback;

/**
 * Created by Juli on 20.06.2014.
 */
public interface ViewPagerListener {

    void nextPage(boolean smoothScroll);

    void previousPage(boolean smoothScroll);
}
