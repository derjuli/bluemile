package de.glassbox.bluemile.ui.fragment.dialog;

import android.app.Activity;
import android.content.DialogInterface;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import de.glassbox.bluemile.R;
import roboguice.inject.InjectView;

/**
 * Created by Juli on 22.04.2014.
 */
public class InfoDialogFragment extends StyleableDialogFragment implements View.OnClickListener {

    /* Data */

    /* Views */
    @InjectView(R.id.popup_headline)
    private TextView mHeadline;
    @InjectView(R.id.popup_description)
    private TextView mDescription;
    @InjectView(R.id.popup_info_icon)
    private View mIcon;
    @InjectView(R.id.popup_close)
    private View mCloseButton;

    /**
     * Create a new instance of MyDialogFragment, providing "num"
     * as an argument.
     */
    public static InfoDialogFragment newInstance(String headline, String description, int iconID) {
        InfoDialogFragment f = new InfoDialogFragment();

        // Supply num input as an argument.
        Bundle args = new Bundle();
        args.putString("headline", headline);
        args.putString("description", description);
        args.putInt("icon", iconID);
        f.setArguments(args);

        return f;
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        return onCreateView(inflater, container, R.layout.popup_info, savedInstanceState);
    }

    @Override
    public void onViewCreated(View view, Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        // load data
        mHeadline.setText(getArguments().getString("headline"));
        mDescription.setText(getArguments().getString("description"));
        mIcon.setBackgroundResource(getArguments().getInt("icon"));

        mCloseButton.setOnClickListener(this);
    }

    @Override
    public void onDismiss(DialogInterface dialog) {
        super.onDismiss(dialog);

        Fragment caller = getTargetFragment();
        if (caller != null) {
            caller.onActivityResult(getTargetRequestCode(), Activity.RESULT_OK, null);
        }
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.popup_close:
                this.dismiss();
                return;
        }
    }
}
