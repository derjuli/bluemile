package de.glassbox.bluemile.ui.fragment;


import android.animation.LayoutTransition;
import android.os.Bundle;
import android.os.Handler;
import android.os.SystemClock;
import android.support.v4.app.FragmentTransaction;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.ViewTreeObserver;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.common.api.ResultCallback;
import com.google.android.gms.games.Games;
import com.google.android.gms.games.leaderboard.LeaderboardVariant;
import com.google.android.gms.games.leaderboard.Leaderboards;
import com.google.android.gms.games.leaderboard.ScoreSubmissionData;
import com.google.inject.Inject;
import com.nirhart.parallaxscroll.views.ParallaxScrollView;

import java.text.DateFormat;
import java.util.List;
import java.util.Locale;

import de.glassbox.bluemile.R;
import de.glassbox.bluemile.model.BMLocation;
import de.glassbox.bluemile.model.BMTrackResult;
import de.glassbox.bluemile.persistence.DataAccess;
import de.glassbox.bluemile.persistence.DataStore;
import de.glassbox.bluemile.ui.callback.GoogleApiProvider;
import de.glassbox.bluemile.utils.DateUtils;
import roboguice.fragment.RoboFragment;
import roboguice.inject.InjectView;

public class HistoryDetailFragment extends RoboFragment implements View.OnClickListener, ResultCallback<Leaderboards.SubmitScoreResult> {

    private static final String PARAM_TRACK = "track";
    private static final String PARAM_RESULT = "new_result";
    @InjectView(R.id.scrollview) ParallaxScrollView mScrollView;
    // start - end
    @InjectView(R.id.tour_detail_start_city) TextView mStartCity;
    @InjectView(R.id.tour_detail_start_street) TextView mStartStreet;
    @InjectView(R.id.tour_detail_dest_city) TextView mEndCity;
    @InjectView(R.id.tour_detail_dest_street) TextView mEndStreet;
    // details
    @InjectView(R.id.tour_detail_time) TextView mTime;
    @InjectView(R.id.tour_detail_avgspeed) TextView mAvgSpeed;
    @InjectView(R.id.tour_detail_maxspeed) TextView mMaxSpeed;
    @InjectView(R.id.tour_detail_distance_covered) TextView mDistanceCovered;
    @InjectView(R.id.tour_detail_time_start) TextView mStartTime;
    @InjectView(R.id.tour_detail_time_end) TextView mEndTime;
    // bars
    @InjectView(R.id.tour_detail_container_bar) LinearLayout mSpeedBarContainer;
    @InjectView(R.id.tour_detail_avgspeed_container) View mAvgSpeedContainer;
    @InjectView(R.id.tour_detail_maxspeed_bars) TextView mMaxSpeedBars;
    // map
    @InjectView(R.id.map) View mMap;
    @InjectView(R.id.scores_new) ViewGroup mScoresNew;
    @InjectView(R.id.scores_new_popup) ViewGroup mScoresNewPopup;
    CustomMapFragment mMapFragment;
    // Data
    @Inject DataAccess mDataAccess;
    @Inject DataStore mDataStore;
    BMTrackResult mTrack;
    boolean mNewResult;
    long mNextAddTimestamp;
    @Inject Handler mHandler;
    // Views
    private View mFragmentView;


    public HistoryDetailFragment() {
    }

    public static HistoryDetailFragment newInstance(BMTrackResult trackResult, boolean newResult) {
        HistoryDetailFragment fragment = new HistoryDetailFragment();
        Bundle bundle = new Bundle();
        bundle.putParcelable(PARAM_TRACK, trackResult);
        bundle.putBoolean(PARAM_RESULT, newResult);
        fragment.setArguments(bundle);
        return fragment;
    }

    @Override
    public void onCreate(final Bundle savedInstanceState) {
        super.onCreate(null);

        if (getArguments() != null) {
            final Bundle b = getArguments();
            mTrack = b.getParcelable(PARAM_TRACK);
            mNewResult = b.getBoolean(PARAM_RESULT);
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        mFragmentView = inflater.inflate(R.layout.fragment_history_detail, null);

        if (savedInstanceState == null) {
            mMapFragment = new CustomMapFragment();
            mMapFragment.setShowStartEndMarkers(true);
            FragmentTransaction transaction = getChildFragmentManager().beginTransaction();
            transaction.add(R.id.map, mMapFragment);
            transaction.commit();

            // set stats
            //            Bundle data = new Bundle();
            //            data.putInt("km", (int) mTrack.getDistanceKM());
            //            data.putInt("savedMiles", (int) mTrack.getSavedMiles());
            //            data.putInt("kmUrban", (int) mTrack.getKmUrban());
            //            data.putInt("kmRural", (int) mTrack.getKmRural());
            //            data.putInt("kmHighway", (int) mTrack.getKmHighway());
            //            data.putInt("kmDay", (int) mTrack.getKmDay());
            //            data.putInt("kmNight", (int) mTrack.getKmNight());
            //            if (tourStatsFragment != null) {
            //                tourStatsFragment.setArguments(data);
            //            }
        }


        return mFragmentView;
    }

    @Override
    public void onViewCreated(View view, Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        // arrange speed bars
        mSpeedBarContainer.setWeightSum(mTrack.getAvgSpeeds().length);
        ViewTreeObserver vt1 = mSpeedBarContainer.getViewTreeObserver();
        vt1.addOnGlobalLayoutListener(new ViewTreeObserver.OnGlobalLayoutListener() {
            @Override
            public void onGlobalLayout() {
                mSpeedBarContainer.getViewTreeObserver().removeGlobalOnLayoutListener(this);


                int maxSpeed = 0;
                for (int i = 0; i < mTrack.getAvgSpeeds().length; i++) {
                    maxSpeed = mTrack.getAvgSpeeds()[i] > maxSpeed ? mTrack.getAvgSpeeds()[i] : maxSpeed;
                }
                maxSpeed = maxSpeed - maxSpeed % 50 + 50;
                mMaxSpeedBars.setText(String.format(getString(R.string.tour_detail_maxspeed_bar), maxSpeed));

                // set bar height of track speeds
                float height = mSpeedBarContainer.getHeight() / 8f * 7;
                for (int i = 0; i < mTrack.getAvgSpeeds().length; i++) {
                    View child = new View(getActivity());
                    child.setBackgroundResource(R.drawable.tour_detail_analysis);
                    LinearLayout.LayoutParams params = new LinearLayout.LayoutParams(LinearLayout.LayoutParams.MATCH_PARENT, LinearLayout.LayoutParams.MATCH_PARENT, 1.0f);
                    params.height = (int) (Math.min(1, (mTrack.getAvgSpeeds()[i] / (float) maxSpeed)) * height);
                    child.setLayoutParams(params);
                    mSpeedBarContainer.addView(child);
                }

                // set avg speed indicator
                RelativeLayout.LayoutParams lp = (RelativeLayout.LayoutParams) mAvgSpeedContainer.getLayoutParams();
                lp.bottomMargin = (int) Math.max(0, (Math.min(1, mTrack.getAvgSpeed() / maxSpeed) * height - mAvgSpeedContainer.getHeight() / 2f));
                mAvgSpeedContainer.setLayoutParams(lp);
            }
        });

        // set detail text
        mStartCity.setText(mTrack.getStartAddressCity());
        mStartStreet.setText(mTrack.getStartAddressStreet());
        mEndCity.setText(mTrack.getEndAddressCity());
        mEndStreet.setText(mTrack.getEndAddressStreet());

        mTime.setText(DateUtils.formatElapsedTime((int) mTrack.getDurationInSeconds(), true));
        mAvgSpeed.setText(String.format(getString(R.string.tour_detail_speed), (int) mTrack.getAvgSpeed()));
        mMaxSpeed.setText(String.format(getString(R.string.tour_detail_speed), (int) mTrack.getMaxSpeed()));

        DateFormat dateFormat = DateFormat.getTimeInstance(DateFormat.SHORT, Locale.getDefault());
        mStartTime.setText(dateFormat.format(mTrack.getStartTime()));
        mEndTime.setText(dateFormat.format(mTrack.getEndTime()));

        mDistanceCovered.setText("" + (int) mTrack.getDistanceKM());

        // submit score if necessary
        if (mNewResult) {
            GoogleApiClient client = ((GoogleApiProvider) getActivity()).getGoogleApiClient();

            LayoutTransition transition = mScoresNewPopup.getLayoutTransition();
            transition.setAnimator(LayoutTransition.APPEARING, null);

            int score = 100;
            Games.Leaderboards.submitScoreImmediate(client, getString(R.string.blue_total_id), score).setResultCallback(this);
            Games.Leaderboards.submitScoreImmediate(client, getString(R.string.blue_avg_id), score).setResultCallback(this);
            Games.Leaderboards.submitScoreImmediate(client, getString(R.string.red_total_id), score).setResultCallback(this);
            Games.Leaderboards.submitScoreImmediate(client, getString(R.string.red_avg_id), score).setResultCallback(this);
            Games.Leaderboards.submitScoreImmediate(client, getString(R.string.combined_total_id), score).setResultCallback(this);
            Games.Leaderboards.submitScoreImmediate(client, getString(R.string.combined_avg_id), score).setResultCallback(this);
        }

        this.drawLocationsOnMap();
    }

    @Override
    public void onResult(Leaderboards.SubmitScoreResult submitScoreResult) {
        String leaderboardId = submitScoreResult.getScoreData().getLeaderboardId();
        if (org.apache.commons.lang3.StringUtils.equals(leaderboardId, getString(R.string.blue_total_id))) {
            addNewHighscore(submitScoreResult, R.string.blue_total);
            return;
        }
        if (org.apache.commons.lang3.StringUtils.equals(leaderboardId, getString(R.string.blue_avg_id))) {
            addNewHighscore(submitScoreResult, R.string.blue_avg);
            return;
        }
        if (org.apache.commons.lang3.StringUtils.equals(leaderboardId, getString(R.string.red_total_id))) {
            addNewHighscore(submitScoreResult, R.string.red_total);
            return;
        }
        if (org.apache.commons.lang3.StringUtils.equals(leaderboardId, getString(R.string.red_avg_id))) {
            addNewHighscore(submitScoreResult, R.string.red_avg);
            return;
        }
        if (org.apache.commons.lang3.StringUtils.equals(leaderboardId, getString(R.string.combined_total_id))) {
            addNewHighscore(submitScoreResult, R.string.combined_total);
            return;
        }
        if (org.apache.commons.lang3.StringUtils.equals(leaderboardId, getString(R.string.combined_avg_id))) {
            addNewHighscore(submitScoreResult, R.string.combined_avg);
            return;
        }
    }

    private void addNewHighscore(final Leaderboards.SubmitScoreResult submitScoreResult, final int pointStringId) {
        final ScoreSubmissionData scoreData = submitScoreResult.getScoreData();
        final ScoreSubmissionData.Result scoreResult = scoreData.getScoreResult(LeaderboardVariant.TIME_SPAN_ALL_TIME);

        //if (scoreResult.newBest) { // TODO only show new highscore
        if (true) {
            mNextAddTimestamp = SystemClock.uptimeMillis() + (mNextAddTimestamp == 0 ? 0 : 700);
            mHandler.postAtTime(new Runnable() {
                @Override
                public void run() {
                    View itemPopup = LayoutInflater.from(getActivity()).inflate(R.layout.listitem_score_new, null);
                    ((TextView) itemPopup.findViewById(R.id.score_points)).setText(getString(pointStringId) + ": " + String.format(getString(R.string.points), scoreResult.rawScore));
                    itemPopup.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View v) {
                            mScrollView.smoothScrollTo(0, mScoresNew.getBottom());
                        }
                    });
                    mScoresNewPopup.addView(itemPopup);

                    View item = LayoutInflater.from(getActivity()).inflate(R.layout.listitem_score_new, null);
                    ((TextView) item.findViewById(R.id.score_points)).setText(getString(pointStringId) + ": " + String.format(getString(R.string.points), scoreResult.rawScore));
                    mScoresNew.addView(item);
                }
            }, mNextAddTimestamp);
            mHandler.postAtTime(new Runnable() {
                @Override
                public void run() {
                    mScoresNewPopup.removeViewAt(0);
                }
            }, mNextAddTimestamp + 2000);
        }
    }

    @Override
    public void onStop() {
        super.onStop();
        mHandler.removeCallbacksAndMessages(null);
    }

    private void drawLocationsOnMap() {
        mDataAccess.getLocationsAsync(mTrack.getLocalId(), new DataAccess.DeliverResult<List<BMLocation>>() {
            @Override
            public void onResult(List<BMLocation> data) {
                mMapFragment.setLocationsToDraw(data);
            }
        }, false);
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
        }
    }

}
