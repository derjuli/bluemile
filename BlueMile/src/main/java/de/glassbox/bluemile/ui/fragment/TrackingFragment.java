package de.glassbox.bluemile.ui.fragment;

import android.content.ComponentName;
import android.content.Context;
import android.content.Intent;
import android.content.ServiceConnection;
import android.os.Bundle;
import android.os.Handler;
import android.os.IBinder;
import android.os.SystemClock;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.ViewStub;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.ImageSwitcher;
import android.widget.ImageView;
import android.widget.TextSwitcher;
import android.widget.TextView;
import android.widget.ViewSwitcher;

import com.google.inject.Inject;
import com.squareup.otto.Bus;
import com.squareup.otto.Subscribe;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

import de.glassbox.bluemile.R;
import de.glassbox.bluemile.model.BMTrackResult;
import de.glassbox.bluemile.model.events.BMTrackingUpdateEvent;
import de.glassbox.bluemile.model.events.TrackingEvaluationEvent;
import de.glassbox.bluemile.model.events.TrackingEvent;
import de.glassbox.bluemile.persistence.DataAccess;
import de.glassbox.bluemile.persistence.DataStore;
import de.glassbox.bluemile.services.TrackingEvaluationService;
import de.glassbox.bluemile.services.TrackingService;
import de.glassbox.bluemile.ui.animation.SimpleAnimationListener;
import de.glassbox.bluemile.utils.DateUtils;
import roboguice.fragment.RoboFragment;
import roboguice.inject.InjectResource;
import roboguice.inject.InjectView;

/**
 * Created by Juli on 03.02.14.
 */
public class TrackingFragment extends RoboFragment implements View.OnClickListener, ServiceConnection {

    private static final String TAG = TrackingFragment.class.getSimpleName();

    View mFragmentView;

    // Data
    boolean mIsBound;
    boolean mIsTracking;
    long mStartTime;

    TrackingService mTrackingService;
    TrackingEvaluationService mTrackingEvaluationService;
    ServiceConnection mConnection = this;
    BMTrackResult mActiveTrack;
    List<Integer> mStatusList = new ArrayList<Integer>();

    @Inject DataStore mDataStore;
    @Inject DataAccess mDataAccess;
    @Inject Bus mEventBus;
    @InjectResource(R.string.tracking_distance_format) String mDistanceFormatString;
    @InjectResource(R.array.tracking_evaluation_status) String[] mTrackingEvaluationStatus;
    @InjectView(R.id.tracking_button) ImageSwitcher mImageSwitcher;
    @InjectView(R.id.tracking_duration) TextView mDurationView;
    @InjectView(R.id.tracking_distance) TextView mDistanceView;
    @InjectView(R.id.tracking_speed) TextView mSpeedView;
    @InjectView(R.id.tracking_evaluation_progress) View mEvaluationProgress;
    @InjectView(R.id.tracking_data_container) View mDataContainer;
    @InjectView(R.id.tracking_evaluation_stub) ViewStub mTrackingEvaluationStub;
    @Inject Handler mHandler;
    TextSwitcher mTextSwitcher;

    public static Fragment newInstance(int sectionNumber) {
        TrackingFragment fragment = new TrackingFragment();
        return fragment;
    }

    @Override
    public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
        inflater.inflate(R.menu.global, menu);
        super.onCreateOptionsMenu(menu, inflater);
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        automaticBind();
    }

    @Override
    public void onResume() {
        super.onResume();
        mEventBus.register(this);
    }

    @Override
    public void onPause() {
        super.onPause();
        mEventBus.unregister(this);
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        try {
            doUnbindService();
        } catch (Throwable t) {
            Log.e(TAG, "Failed to unbind from the service", t);
        }
        mHandler.removeCallbacksAndMessages(null);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        mFragmentView = inflater.inflate(R.layout.fragment_tracking, null);
        return mFragmentView;
    }

    @Override
    public void onViewCreated(View view, Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        mImageSwitcher.setFactory(new ViewSwitcher.ViewFactory() {
            @Override
            public View makeView() {
                ImageView myView = new ImageView(getActivity());
                myView.setScaleType(ImageView.ScaleType.FIT_CENTER);
                myView.setLayoutParams(new ImageSwitcher.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.MATCH_PARENT));
                return myView;
            }

        });
        mImageSwitcher.setInAnimation(AnimationUtils.loadAnimation(getActivity(), R.anim.grow_from_center));
        mImageSwitcher.setOutAnimation(AnimationUtils.loadAnimation(getActivity(), R.anim.shrink_from_center));
        mImageSwitcher.setOnClickListener(this);
        mImageSwitcher.setImageResource(R.drawable.ic_tracking_start);
    }

    private void startTracking() {
        mEventBus.post(new TrackingEvent(true));
        mIsTracking = true;
        mImageSwitcher.setImageResource(R.drawable.ic_tracking_stop);
        mDurationView.setText(DateUtils.formatElapsedTime(0, true));
        mDistanceView.setText(String.format(mDistanceFormatString, 0f));
        mSpeedView.setText("0");

        mActiveTrack = new BMTrackResult();
        mActiveTrack.setTrackStatus(BMTrackResult.STATUS_CREATED);
        mDataAccess.storeTrackResultAsync(mActiveTrack, new DataAccess.DeliverResult<BMTrackResult>() {
            @Override
            public void onResult(BMTrackResult storedTrackResult) {
                // starts tracking in a service
                mActiveTrack = storedTrackResult;
                mTrackingService.startTracking(storedTrackResult);
            }
        }, false);

        mStartTime = SystemClock.elapsedRealtime();
        mHandler.postDelayed(new Runnable() {
            @Override
            public void run() {
                mDurationView.setText(DateUtils.formatElapsedTime((SystemClock.elapsedRealtime() - mStartTime) / 1000, true));
                mHandler.postDelayed(this, 1000);
            }
        }, 1000);
    }

    private void stopTracking() {
        mTrackingService.stopTracking();
        mIsTracking = false;

        // fade out image switcher, fade in gif view
        Animation animation = AnimationUtils.loadAnimation(getActivity(), R.anim.shrink_from_center);
        animation.setFillAfter(true);
        mImageSwitcher.startAnimation(animation);
        mEvaluationProgress.setLayoutParams(mImageSwitcher.getLayoutParams());
        mEvaluationProgress.startAnimation(AnimationUtils.loadAnimation(getActivity(), R.anim.grow_from_center));
        mEvaluationProgress.setVisibility(View.VISIBLE);
        mImageSwitcher.setOnClickListener(null);

        // slide out tracking data
        Animation slideOutAnim = AnimationUtils.loadAnimation(getActivity(), R.anim.tracking_data_out);
        slideOutAnim.setAnimationListener(new SimpleAnimationListener() {
            @Override
            public void onAnimationEnd(Animation animation) {
                super.onAnimationEnd(animation);
                mDataContainer.setVisibility(View.GONE);
            }
        });
        mDataContainer.startAnimation(slideOutAnim);
        // create and slide in evaluation status
        View importPanel = mTrackingEvaluationStub.inflate();
        importPanel.startAnimation(AnimationUtils.loadAnimation(getActivity(), R.anim.tracking_data_in));

        // provide two TextViews for the TextSwitcher to use. you can apply styles to these Views before adding
        // inflate layout because style cannot be set programmatically.
        mTextSwitcher = (TextSwitcher) importPanel.findViewById(R.id.tracking_evaluation_text);
        LayoutInflater inflater = (LayoutInflater) getActivity().getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        mTextSwitcher.addView(inflater.inflate(R.layout.evaluation_progress_text, null));
        mTextSwitcher.addView(inflater.inflate(R.layout.evaluation_progress_text, null));

        mEventBus.post(new TrackingEvent(false));
        mTrackingEvaluationService.processTrackResult(mActiveTrack.getLocalId());
//        Intent serviceIntent = new Intent(getActivity(), TrackingEvaluationService.class);
//        serviceIntent.putExtra(TrackingEvaluationService.TRACKING_ID, mActiveTrack.getLocalId());
//        getActivity().startService(serviceIntent);
    }

    @Subscribe
    public void trackingEvaluationUpdate(TrackingEvaluationEvent trackingEvaluationEvent) {
        createEvaluationStatusList();

        BMTrackResult trackResult = trackingEvaluationEvent.getTrackResult();
        if (trackResult != null) {
            if (trackResult.getTrackStatusEnum() == BMTrackResult.TrackStatus.Finished) {
                // finished tracking
                // TODO open summary and submit score
            } else {
                // new status
                mTextSwitcher.setText(mTrackingEvaluationStatus[mStatusList.remove(0)]);
            }
        }
    }

    private void createEvaluationStatusList() {
        if (mStatusList.size() == 0) {
            for (int i = 0; i < mTrackingEvaluationStatus.length; i++) {
                mStatusList.add(i);
                Collections.shuffle(mStatusList);
            }
        }
    }

    @Subscribe
    public void onLocationUpdate(BMTrackingUpdateEvent event) {
        mDistanceView.setText(String.format(mDistanceFormatString, (float) (event.getTotalDistanceMeters() / 1000)));
        mSpeedView.setText(Integer.toString(event.getSpeed()));
    }

    @Override
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.tracking_button:
                if (!mIsTracking) {
                    startTracking();
                } else {
                    stopTracking();
                }
                return;
        }
    }


    /*
    ################################
    ###                          ###
    ###      Service Binding     ###
    ###                          ###
    ################################
     */

    /**
     * Check if the service is running. If the service is running
     * when the activity starts, we want to automatically bind to it.
     */
    private void automaticBind() {
        if (TrackingService.isRunning()) {
            getActivity().startService(new Intent(getActivity().getApplicationContext(), TrackingService.class));
        }
        if (TrackingEvaluationService.isRunning()) {
            getActivity().startService(new Intent(getActivity().getApplicationContext(), TrackingEvaluationService.class));
        }

        getActivity().getApplicationContext().bindService(new Intent(getActivity().getApplicationContext(), TrackingService.class), mConnection, Context.BIND_AUTO_CREATE);
        getActivity().getApplicationContext().bindService(new Intent(getActivity().getApplicationContext(), TrackingEvaluationService.class), mConnection, Context.BIND_AUTO_CREATE);
        mIsBound = true;
    }

    /**
     * Un-bind this Activity to MyService
     */
    private void doUnbindService() {
        if (mIsBound) {
            // Detach our existing connection.
            getActivity().getApplicationContext().unbindService(mConnection);
            mIsBound = false;
        }
    }

    @Override
    public void onServiceConnected(ComponentName name, IBinder service) {
        Log.d(TAG, "Attached.");

        if (service instanceof TrackingService.TrackingServiceBinder) {
            TrackingService.TrackingServiceBinder binder = (TrackingService.TrackingServiceBinder) service;
            mTrackingService = binder.getService();
        } else {
            TrackingEvaluationService.TrackingEvaluationServiceBinder binder = (TrackingEvaluationService.TrackingEvaluationServiceBinder) service;
            mTrackingEvaluationService = binder.getService();
        }
    }

    @Override
    public void onServiceDisconnected(ComponentName name) {
        // This is called when the connection with the service has been unexpectedly disconnected - process crashed.
        mIsBound = false;
    }

}
