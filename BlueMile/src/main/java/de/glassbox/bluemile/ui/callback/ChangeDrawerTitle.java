package de.glassbox.bluemile.ui.callback;

/**
 * Created by Juli on 02.07.2014.
 */
public interface ChangeDrawerTitle {

    void onSectionAttached(int number);

    void restoreActionBar();
}
