package de.glassbox.bluemile.ui.activities;

import android.os.Bundle;

import de.glassbox.bluemile.R;
import de.glassbox.bluemile.model.BMTrackResult;
import de.glassbox.bluemile.ui.fragment.HistoryDetailFragment;
import roboguice.activity.RoboFragmentActivity;
import roboguice.inject.ContentView;


@ContentView(R.layout.activity_history_detail)
public class HistoryDetailActivity extends RoboFragmentActivity {

    public static final String EXTRA_TRACK_RESULT = "track_result";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        if (savedInstanceState == null) {
            Bundle bundle = getIntent().getExtras();
            BMTrackResult trackResult = bundle.getParcelable(EXTRA_TRACK_RESULT);
            getSupportFragmentManager().beginTransaction().add(R.id.container, HistoryDetailFragment.newInstance(trackResult, false)).commit();
        }
    }


    //    @Override
    //    public boolean onCreateOptionsMenu(Menu menu) {
    //        // Inflate the menu; this adds items to the action bar if it is present.
    //        getMenuInflater().inflate(R.menu.history_detail, menu);
    //        return true;
    //    }
    //
    //    @Override
    //    public boolean onOptionsItemSelected(MenuItem item) {
    //        // Handle action bar item clicks here. The action bar will
    //        // automatically handle clicks on the Home/Up button, so long
    //        // as you specify a parent activity in AndroidManifest.xml.
    //        int id = item.getItemId();
    //        if (id == R.id.action_settings) {
    //            return true;
    //        }
    //        return super.onOptionsItemSelected(item);
    //    }
}
