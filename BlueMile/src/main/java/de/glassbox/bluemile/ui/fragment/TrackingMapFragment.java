package de.glassbox.bluemile.ui.fragment;


import android.app.Fragment;
import android.os.Bundle;
import android.support.v4.app.FragmentTransaction;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.View;
import android.view.ViewGroup;

import com.google.inject.Inject;
import com.squareup.otto.Bus;
import com.squareup.otto.Subscribe;

import java.util.ArrayList;
import java.util.List;

import de.glassbox.bluemile.R;
import de.glassbox.bluemile.model.BMLocation;
import de.glassbox.bluemile.model.events.BMTrackingUpdateEvent;
import de.glassbox.bluemile.model.events.LocationEvent;
import roboguice.fragment.RoboFragment;

/**
 * A simple {@link Fragment} subclass.
 * Use the {@link TrackingMapFragment#newInstance} factory method to
 * create an instance of this fragment.
 */
public class TrackingMapFragment extends RoboFragment {
    // the fragment initialization parameters, e.g. ARG_ITEM_NUMBER
    //private static final String ARG_PARAM1 = "param1";
    private static final String TAG = TrackingMapFragment.class.getSimpleName();
    private static final float MAX_UPDATE_DISTANCE_METERS = 400;

    CustomMapFragment mMapFragment;
    int mUpdateDistanceCounter;
    List<BMLocation> mLocations = new ArrayList<BMLocation>();
    @Inject Bus mEventBus;

    public TrackingMapFragment() {
        // Required empty public constructor
    }

    public static TrackingMapFragment newInstance() {
        TrackingMapFragment fragment = new TrackingMapFragment();
        Bundle args = new Bundle();
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
        }
    }


    @Override
    public void onResume() {
        super.onResume();
        mEventBus.register(this);
    }

    @Override
    public void onPause() {
        super.onPause();
        mEventBus.unregister(this);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        if (savedInstanceState == null) {
            mMapFragment = new CustomMapFragment();
            mMapFragment.setShowUserLocation(true);
            FragmentTransaction transaction = getChildFragmentManager().beginTransaction();
            transaction.add(R.id.map, mMapFragment);
            transaction.commit();
        }

        return inflater.inflate(R.layout.fragment_tracking_map, container, false);
    }

    public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
        inflater.inflate(R.menu.tracking, menu);
        super.onCreateOptionsMenu(menu, inflater);
    }

    @Subscribe
    public void onLocationUpdate(BMTrackingUpdateEvent event) {
        // this will be called every time there is a location update event while tracking
        mLocations.add(event.getLocation());
        int count = (int) (event.getTotalDistanceMeters() / MAX_UPDATE_DISTANCE_METERS);
        if (count > mUpdateDistanceCounter) {
            mUpdateDistanceCounter = count;
            mMapFragment.drawLine(new ArrayList<BMLocation>(mLocations));
            mLocations.clear();
            //mMapFragment.setUserLocation(event.getLocation());
        }
    }

    @Subscribe
    public void onLocationEvent(LocationEvent event) {
        // this will be called at the start of the fragment to zoom the map to the users position
        if (mMapFragment != null) {
            mMapFragment.setUserLocation(event.getLocation());
        }
    }


}
