package de.glassbox.bluemile.ui.adapters;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TextView;

import java.util.List;

import de.glassbox.bluemile.R;
import de.glassbox.bluemile.model.CarPart;
import de.glassbox.bluemile.model.CarStat;

/**
 * Created by Juli on 22.06.2014.
 */
public class CarPartsAdapter extends ArrayAdapter<CarPart> {

    final String[] mCarStats;

    public CarPartsAdapter(Context context, List<CarPart> items) {
        super(context, 0, items);
        mCarStats = getContext().getResources().getStringArray(R.array.car_stats);
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        CarPartHolder holder;

        if (convertView == null) {
            convertView = LayoutInflater.from(getContext()).inflate(R.layout.carpart_item, null);
            holder = new CarPartHolder();

            holder.title = (TextView) convertView.findViewById(R.id.title);
            holder.stat1 = (TextView) convertView.findViewById(R.id.stat1);
            holder.stat2 = (TextView) convertView.findViewById(R.id.stat2);
            holder.stat3 = (TextView) convertView.findViewById(R.id.stat3);
            holder.purchase = (TextView) convertView.findViewById(R.id.purchase);

            convertView.setTag(holder);
        } else {
            holder = (CarPartHolder) convertView.getTag();
        }

        CarPart carPart = getItem(position);
        holder.title.setText(carPart.getName());

        int statAmount = carPart.getCarStats().size();
        CarStat carStat;

        carStat = statAmount > 0 ? carPart.getCarStats().get(0) : null;
        holder.stat1.setText(carStat != null ? String.format(mCarStats[carStat.getType().ordinal()], carStat.getAmount()) : null);

        carStat = statAmount > 1 ? carPart.getCarStats().get(1) : null;
        holder.stat2.setText(carStat != null ? String.format(mCarStats[carStat.getType().ordinal()], carStat.getAmount()) : null);

        carStat = statAmount > 2 ? carPart.getCarStats().get(2) : null;
        holder.stat3.setText(carStat != null ? String.format(mCarStats[carStat.getType().ordinal()], carStat.getAmount()) : null);

        holder.purchase.setText("" + (carPart.getCostBlue() > 0 ? carPart.getCostBlue() : carPart.getCostRed()));
        holder.purchase.setCompoundDrawablesWithIntrinsicBounds(carPart.getCostBlue() > 0 ? R.drawable.point_blue : R.drawable.point_red, 0, 0, 0);

        return convertView;
    }

    private static class CarPartHolder {
        TextView title;
        TextView stat1;
        TextView stat2;
        TextView stat3;
        TextView purchase;
    }
}
