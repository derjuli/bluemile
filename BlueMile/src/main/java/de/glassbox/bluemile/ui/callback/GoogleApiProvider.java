package de.glassbox.bluemile.ui.callback;

import com.google.android.gms.common.api.GoogleApiClient;

/**
 * Created by Juli on 28.06.2014.
 */
public interface GoogleApiProvider {

        public GoogleApiClient getGoogleApiClient();
}
