package de.glassbox.bluemile.ui.activities;

import android.os.Bundle;

import de.glassbox.bluemile.R;
import de.glassbox.bluemile.ui.fragment.TrackingFragment;
import roboguice.activity.RoboFragmentActivity;

public class TrackingActivity extends RoboFragmentActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_tracking);
        if (savedInstanceState == null) {
            getSupportFragmentManager().beginTransaction().add(R.id.container, TrackingFragment.newInstance(0)).commit();
        }
    }

}
