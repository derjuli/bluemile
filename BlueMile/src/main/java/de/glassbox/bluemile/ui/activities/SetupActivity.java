package de.glassbox.bluemile.ui.activities;

import android.os.Bundle;

import de.glassbox.bluemile.R;
import de.glassbox.bluemile.ui.fragment.SetupFragment;
import roboguice.activity.RoboFragmentActivity;


public class SetupActivity extends RoboFragmentActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_setup);
        if (savedInstanceState == null) {
            getSupportFragmentManager().beginTransaction().add(R.id.container, new SetupFragment()).commit();
        }
    }

}
