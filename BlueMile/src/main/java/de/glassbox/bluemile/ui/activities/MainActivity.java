package de.glassbox.bluemile.ui.activities;

import android.app.ActionBar;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.support.v4.view.ViewPager;
import android.support.v4.widget.DrawerLayout;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.Toast;

import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.games.Games;
import com.google.android.gms.games.Player;
import com.google.example.games.basegameutils.BaseGameActivity;
import com.google.inject.Inject;
import com.squareup.otto.Bus;
import com.squareup.otto.Subscribe;

import de.glassbox.bluemile.R;
import de.glassbox.bluemile.model.BMTrackResult;
import de.glassbox.bluemile.model.events.TrackingEvaluationEvent;
import de.glassbox.bluemile.model.events.TrackingEvent;
import de.glassbox.bluemile.ui.callback.ChangeDrawerTitle;
import de.glassbox.bluemile.ui.callback.GoogleApiProvider;
import de.glassbox.bluemile.ui.fragment.GarageFragment;
import de.glassbox.bluemile.ui.fragment.HistoryDetailFragment;
import de.glassbox.bluemile.ui.fragment.HistoryFragment;
import de.glassbox.bluemile.ui.fragment.LeaderboardSelectionFragment;
import de.glassbox.bluemile.ui.fragment.NavigationDrawerFragment;
import de.glassbox.bluemile.ui.fragment.StartFragment;
import roboguice.inject.ContentView;
import roboguice.inject.InjectResource;
import roboguice.inject.InjectView;

@ContentView(R.layout.activity_main)
public class MainActivity extends BaseGameActivity implements NavigationDrawerFragment.NavigationDrawerCallbacks, GoogleApiProvider, ChangeDrawerTitle {

    private static final String TAG = MainActivity.class.getSimpleName();

    /**
     * Used to store the last screen title. For use in {@link #restoreActionBar()}.
     */
    CharSequence mTitle;
    boolean drawerEnabled = true;
    NavigationDrawerFragment mNavigationDrawerFragment;
    @Inject Bus mEventBus;
    @InjectResource(R.array.sidebar) String[] mSidebarContent;
    @InjectView(R.id.drawer_layout) DrawerLayout mDrawerLayout;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        mNavigationDrawerFragment = (NavigationDrawerFragment) getSupportFragmentManager().findFragmentById(R.id.navigation_drawer);
        mTitle = getTitle();
        // Set up the drawer.
        mNavigationDrawerFragment.setUp(R.id.navigation_drawer, mDrawerLayout);

        // restore or create fragment
        if (savedInstanceState == null) {
        }
    }

    @Override
    protected void onResume() {
        super.onResume();
        mEventBus.register(this);
    }

    @Override
    protected void onPause() {
        super.onPause();
        mEventBus.unregister(this);
    }

    @Subscribe
    public void trackingStatusChanged(TrackingEvent trackingEvent) {
        mDrawerLayout.setDrawerLockMode(trackingEvent.isTrackingStartet() ? DrawerLayout.LOCK_MODE_LOCKED_CLOSED : DrawerLayout.LOCK_MODE_UNLOCKED);
        drawerEnabled = !trackingEvent.isTrackingStartet();
    }

    @Override
    public void onNavigationDrawerItemSelected(int position) {
        // update the main content by replacing fragments
        ViewPager pager = (ViewPager) findViewById(R.id.pager);
        if (pager != null) {
            pager.setCurrentItem(position, true);
        }

        setTitle(mSidebarContent[position]);
        getActionBar().setTitle(mSidebarContent[position]);

        if (position > 0) {
            clearBackstack();
            replaceMainFragment(R.id.container, getItem(position), false, true);
        } else {
            replaceMainFragment(R.id.container, getItem(position));
        }
    }

    private void clearBackstack() {
        // clear back stack, so only the latest fragment is on the back stack
        if (getSupportFragmentManager().getBackStackEntryCount() > 0) {
            getSupportFragmentManager().popBackStackImmediate(null, FragmentManager.POP_BACK_STACK_INCLUSIVE);
        }
    }

    public Fragment getItem(int position) {
        Fragment fragment = getSupportFragmentManager().findFragmentById(R.id.container);
        switch (position) {
            case 0:
                if (fragment instanceof StartFragment) {
                    return null;
                }
                return StartFragment.newInstance(position);
            case 1:
                if (fragment instanceof GarageFragment) {
                    return null;
                }
                return GarageFragment.newInstance(position);
            case 2:
                if (fragment instanceof LeaderboardSelectionFragment) {
                    return null;
                }
                return LeaderboardSelectionFragment.newInstance(position);
            case 3:
                if (fragment instanceof HistoryFragment) {
                    return null;
                }
                return HistoryFragment.newInstance(position);
        }
        return null;
    }

    private void replaceMainFragment(int containerID, Fragment fragment) {
        replaceMainFragment(containerID, fragment, false);
    }

    private void replaceMainFragment(int containerID, Fragment fragment, boolean allowStateLoss) {
        replaceMainFragment(containerID, fragment, allowStateLoss, false);
    }

    private void replaceMainFragment(int containerID, Fragment fragment, boolean allowStateLoss, boolean addToBackstack) {
        if (fragment == null) {
            return;
        }
        Log.d(TAG, "replacing old fragment with " + fragment.toString());
        // Create stop fragment and replace current one.
        FragmentTransaction ft = getSupportFragmentManager().beginTransaction();
        //ft.setCustomAnimations(R.anim.slide_in_right, R.anim.slide_out_left, R.anim.slide_in_left, R.anim.slide_out_right);
        ft.replace(containerID, fragment);
        if (addToBackstack) {
            ft.addToBackStack(null);
        }

        if (allowStateLoss) {
            ft.commitAllowingStateLoss();
        } else {
            ft.commit();
        }
    }

    @Subscribe
    public void trackingEvaluationUpdate(TrackingEvaluationEvent trackingEvaluationEvent) {
        if (trackingEvaluationEvent.getTrackResult().getTrackStatusEnum() == BMTrackResult.TrackStatus.Finished) {
            Toast.makeText(this, "yippee aye yay", Toast.LENGTH_SHORT).show();
            clearBackstack();
            replaceMainFragment(R.id.container, HistoryDetailFragment.newInstance(trackingEvaluationEvent.getTrackResult(), true), false, true);
        }
    }

    @Override
    public void onSectionAttached(int number) {
        mTitle = mSidebarContent[number];
        getActionBar().setTitle(mTitle);
    }

    @Override
    public void restoreActionBar() {
        ActionBar actionBar = getActionBar();
        actionBar.setNavigationMode(ActionBar.NAVIGATION_MODE_STANDARD);
        actionBar.setDisplayShowTitleEnabled(true);
        actionBar.setTitle(mTitle);
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        if (!mNavigationDrawerFragment.isDrawerOpen()) {
            // Only show items in the action bar relevant to this screen
            // if the drawer is not showing. Otherwise, let the drawer
            // decide what to show in the action bar.
            getMenuInflater().inflate(R.menu.main, menu);
            restoreActionBar();
            return true;
        }

        return super.onCreateOptionsMenu(menu);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();
        if (id == R.id.action_settings) {
            return true;
        }

        return drawerEnabled ? super.onOptionsItemSelected(item) : true;
    }


    @Override
    public void onSignInFailed() {

    }

    @Override
    public void onSignInSucceeded() {
        // Set the greeting appropriately on main menu
        Player p = Games.Players.getCurrentPlayer(getApiClient());
        String displayName;
        if (p == null) {
            Log.w(TAG, "mGamesClient.getCurrentPlayer() is NULL!");
            displayName = "???";
        } else {
            displayName = p.getDisplayName();
        }
    }

    @Override
    public GoogleApiClient getGoogleApiClient() {
        return getApiClient();
    }
}
