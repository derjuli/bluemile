package de.glassbox.bluemile.ui.views;

import android.content.Context;
import android.graphics.Canvas;
import android.graphics.drawable.Drawable;
import android.util.AttributeSet;
import android.widget.ImageView;

public class OffsetImageView extends ImageView {

    private float mDefaultX;
    private float mOffsetX;
    private float mOffsetY;

    public OffsetImageView(Context context) {
        this(context, null);
    }

    public OffsetImageView(Context context, AttributeSet attributeset) {
        this(context, attributeset, 0);
    }

    public OffsetImageView(Context context, AttributeSet attributeset, int i) {
        super(context, attributeset, i);
        mDefaultX = 0.0F;
        mOffsetX = 0.0F;
        mOffsetY = 0.0F;
    }

    public float getDefaultOffsetX() {
        return mDefaultX;
    }

    public float getOffsetX() {
        return mOffsetX;
    }

    public float getOffsetY() {
        return mOffsetY;
    }

    protected void onDraw(Canvas canvas) {
        canvas.translate(mDefaultX + mOffsetX, mOffsetY);
        super.onDraw(canvas);
    }

    public void setDefaultOffsetX(float f) {
        mDefaultX = f;
        invalidate();
    }

    public void setImageDrawable(Drawable drawable) {
        super.setImageDrawable(drawable);
    }

    public void setOffsetX(float f) {
        mOffsetX = f;
        invalidate();
    }

    public void setOffsetY(float f) {
        mOffsetY = f;
        invalidate();
    }
}
