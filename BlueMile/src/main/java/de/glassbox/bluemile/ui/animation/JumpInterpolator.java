package de.glassbox.bluemile.ui.animation;

/**
 * Created by Juli on 20.06.2014.
 */

import android.view.animation.Interpolator;

public class JumpInterpolator implements Interpolator {

    public float getInterpolation(float t) {
        float x = (float) Math.sin(t * Math.PI);
        return x * x;
    }

}