package de.glassbox.bluemile.ui.activities;

import android.app.Activity;
import android.content.Intent;
import android.content.res.TypedArray;
import android.graphics.BitmapFactory;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;
import android.support.v4.view.ViewPager;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.ViewTreeObserver;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.ImageView;
import android.widget.TextView;

import com.google.inject.Inject;

import de.glassbox.bluemile.App;
import de.glassbox.bluemile.R;
import de.glassbox.bluemile.model.User;
import de.glassbox.bluemile.persistence.DataAccess;
import de.glassbox.bluemile.persistence.DataStore;
import de.glassbox.bluemile.ui.animation.JumpInterpolator;
import de.glassbox.bluemile.ui.animation.SimpleAnimationListener;
import de.glassbox.bluemile.ui.callback.ViewPagerListener;
import de.glassbox.bluemile.ui.callback.ViewPagerListenerImpl;
import de.glassbox.bluemile.ui.views.OffsetImageView;
import roboguice.activity.RoboFragmentActivity;
import roboguice.inject.ContentView;
import roboguice.inject.InjectView;

@ContentView(R.layout.activity_intro)
public class LoginActivity extends RoboFragmentActivity implements ViewPager.OnPageChangeListener, ViewPagerListener {

    static final String TAG = LoginActivity.class.getSimpleName();

    @Inject DataAccess mDataAccess;
    @Inject DataStore mDataStore;
    //@Inject AbstractRestService mRestService;
    @InjectView(R.id.pager) ViewPager mViewPager;
    @InjectView(R.id.background) OffsetImageView mBackground;

    User mUser;
    IntroPagerAdapter mAdapter;
    ViewPagerListenerImpl mPagerListener;
    float mScaledWidth;
    float mOffsetX;
    float mOffsetY;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        // check if the user already logged in and start app or intro
        // TODO don't do this on ui thread
        if (App.sSkipIntro || loadUser()) {
            startMainApp();
            return;
        }

        mAdapter = new IntroPagerAdapter(getSupportFragmentManager());
        mViewPager.setAdapter(mAdapter);
        mViewPager.setOffscreenPageLimit(10);
        mViewPager.setPageTransformer(false, new ViewPager.PageTransformer() {
            @Override
            public void transformPage(View page, float position) {
                float normalizedposition = Math.abs(Math.abs(position) - 1);
                normalizedposition *= normalizedposition;
                page.setAlpha(normalizedposition);

                final int offset = 120;
                ViewGroup container = (ViewGroup) page.findViewById(R.id.intro_container);
                container.getChildAt(0).setTranslationX(position * offset);
                container.getChildAt(2).setTranslationX(position * offset * 3f);
            }
        });
        mViewPager.setOnPageChangeListener(this);
        mPagerListener = new ViewPagerListenerImpl(mViewPager);

        boolean flag = true;
        final BitmapFactory.Options options = new BitmapFactory.Options();
        options.inJustDecodeBounds = flag;
        BitmapFactory.decodeResource(getResources(), R.drawable.splash, options);

        ViewTreeObserver vt = mBackground.getViewTreeObserver();
        vt.addOnGlobalLayoutListener(new ViewTreeObserver.OnGlobalLayoutListener() {
            @Override
            public void onGlobalLayout() {
                mBackground.getViewTreeObserver().removeGlobalOnLayoutListener(this);
                mScaledWidth = ((float) mBackground.getHeight() / (float) options.outHeight) * (float) options.outWidth;
                mOffsetX = (mScaledWidth - mBackground.getWidth()) / (-1 + mAdapter.getCount());
                mOffsetY = options.outHeight * ((mBackground.getScaleY() - 1) / 2 + 1) - options.outHeight;
                mBackground.setDefaultOffsetX((mScaledWidth - (float) mBackground.getWidth()) / 2.0F - 1.0F);
            }
        });
    }

    private boolean loadUser() {
        mUser = mDataAccess.getUser(this);
        if (mUser != null) {
            mDataStore.setUser(mUser);
            return true;
        }
        return false;
    }

    private void startMainApp() {
        Intent intent = new Intent(LoginActivity.this, MainActivity.class);
        startActivity(intent);
        finish();
    }

    private void storeUser(User user) {
        mDataStore.setUser(user);
        mDataAccess.storeUser(getApplicationContext(), user);
        startMainApp();
    }

    //    private void skipLogin() {
    //        final User user = new BMUser();
    //        user.setAnonymous(true);
    //        mRestService.createAnonymousAppUser(user, new Response.Listener<User>() {
    //            @Override
    //            public void onResponse(User response) {
    //                storeUser(response);
    //            }
    //        }, new BlueMileErrorListener() {
    //            @Override
    //            protected void onErrorResponse(BlueMileError error) {
    //                ErrorDialogFragment errorDialog = ErrorDialogFragment.newInstance(getString(R.string.error_no_internet_headline),
    //                        getString(R.string.error_no_internet_description));
    //                errorDialog.createPopup(LoginActivity.this, false);
    //            }
    //        });
    //    }

    @Override
    public void onPageScrolled(int position, float positionOffset, int positionOffsetPixels) {
        float offset = (float) position * mOffsetX + positionOffset * mOffsetX;
        mBackground.setOffsetX(-offset);

        // y movement
        if (position == mAdapter.getCount() - 3) {
            mBackground.setOffsetY(positionOffset * mOffsetY);
        }
    }

    @Override
    public void onPageSelected(int position) {

    }

    @Override
    public void onPageScrollStateChanged(int state) {

    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        mBackground.setImageBitmap(null);
    }

    @Override
    public void nextPage(boolean smoothScroll) {
        mPagerListener.nextPage(smoothScroll);
    }

    @Override
    public void previousPage(boolean smoothScroll) {
        mPagerListener.previousPage(smoothScroll);
    }

    /**
     * A {@link android.support.v13.app.FragmentPagerAdapter} that returns a fragment corresponding to
     * one of the sections/tabs/pages.
     */
    public class IntroPagerAdapter extends FragmentPagerAdapter {

        private final String[] mHeadlines;
        private final String[] mDescriptions;
        private final int[] mIcons;

        public IntroPagerAdapter(FragmentManager fm) {
            super(fm);

            mHeadlines = getApplicationContext().getResources().getStringArray(R.array.intro_headlines);
            mDescriptions = getApplicationContext().getResources().getStringArray(R.array.intro_descriptions);

            TypedArray icons = getResources().obtainTypedArray(R.array.intro_icons);
            mIcons = new int[icons.length()];
            for (int i = 0; i < icons.length(); i++) {
                mIcons[i] = icons.getResourceId(i, -1);
            }
            icons.recycle();
        }

        @Override
        public Fragment getItem(int position) {
            Fragment fragment;
            if (position == 0) {
                return WelcomeFragment.newInstance(mHeadlines[position], mDescriptions[position], mIcons[position], R.layout.fragment_intro_welcome);
            }
            if (position == getCount() - 1) {
                return new IntroLoginSelectorFragment();
            }
            //            if (position == getCount() - 1) {
            //                return new IntroLoginFragment();
            //            }
            return IntroFragment.newInstance(mHeadlines[position], mDescriptions[position], mIcons[position], R.layout.fragment_intro_icon);
        }

        @Override
        public int getCount() {
            return mHeadlines.length + 1;
        }

        @Override
        public CharSequence getPageTitle(int position) {
            return null;
        }
    }

    public static class IntroFragment extends Fragment {

        public IntroFragment() {
        }

        public static IntroFragment newInstance(String headline, String description, int iconRes, int layout) {
            IntroFragment fragment = new IntroFragment();
            fragment.setArguments(headline, description, iconRes, layout);

            return fragment;
        }

        public void setArguments(String headline, String description, int iconRes, int layout) {
            Bundle args = new Bundle();
            args.putString("headline", headline);
            args.putString("description", description);
            args.putInt("iconRes", iconRes);
            args.putInt("layout", layout);

            this.setArguments(args);
        }

        @Override
        public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
            Bundle args = getArguments();

            View introView = inflater.inflate(args.getInt("layout"), null);
            ((TextView) introView.findViewById(R.id.intro_headline)).setText(args.getString("headline"));
            ((TextView) introView.findViewById(R.id.intro_description)).setText(args.getString("description"));
            ((ImageView) introView.findViewById(R.id.intro_icon)).setImageResource(args.getInt("iconRes"));

            return introView;
        }

    }

    public static class WelcomeFragment extends IntroFragment {

        public WelcomeFragment() {
        }

        public static WelcomeFragment newInstance(String headline, String description, int iconRes, int layout) {
            WelcomeFragment fragment = new WelcomeFragment();
            fragment.setArguments(headline, description, iconRes, layout);

            return fragment;
        }

        @Override
        public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
            View view = super.onCreateView(inflater, container, savedInstanceState);

            final Animation anim = AnimationUtils.loadAnimation(getActivity(), R.anim.jump_left);
            anim.setAnimationListener(new SimpleAnimationListener() {
                @Override
                public void onAnimationEnd(Animation animation) {
                    anim.reset();
                    anim.start();
                }
            });
            anim.setInterpolator(new JumpInterpolator());
            view.findViewById(R.id.intro_swipe).startAnimation(anim);

            return view;
        }
    }

    public static class IntroLoginSelectorFragment extends Fragment implements View.OnClickListener {

        ViewPagerListener mPagerListener;

        public IntroLoginSelectorFragment() {
        }

        @Override
        public void onAttach(Activity activity) {
            super.onAttach(activity);
            try {
                mPagerListener = (ViewPagerListener) activity;
            } catch (ClassCastException ex) {
            }
        }

        @Override
        public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
            View introView = inflater.inflate(R.layout.fragment_intro_login_select, null);

            introView.findViewById(R.id.login_button).setOnClickListener(this);
            introView.findViewById(R.id.register_button).setOnClickListener(this);
            introView.findViewById(R.id.login_skip).setOnClickListener(this);

            return introView;
        }

        @Override
        public void onClick(View v) {
            switch (v.getId()) {
                case R.id.login_button:
                    mPagerListener.nextPage(true);
                    return;
                case R.id.register_button:
                    return;
                case R.id.login_skip:
                    Intent intent = new Intent(getActivity(), MainActivity.class);
                    startActivity(intent);
                    return;
            }

        }
    }

    public static class IntroLoginFragment extends Fragment implements View.OnClickListener {
        public IntroLoginFragment() {
        }

        @Override
        public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
            View introView = inflater.inflate(R.layout.fragment_intro_login, null);
            introView.findViewById(R.id.login_button).setOnClickListener(this);

            return introView;
        }

        @Override
        public void onClick(View v) {
            switch (v.getId()) {
                case R.id.login_button:
                    // TODO login
                    return;
            }

        }
    }


}
