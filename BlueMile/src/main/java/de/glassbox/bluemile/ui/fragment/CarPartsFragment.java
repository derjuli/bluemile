package de.glassbox.bluemile.ui.fragment;

import android.content.res.Resources;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ListView;

import java.io.InputStream;
import java.util.ArrayList;
import java.util.List;

import de.glassbox.bluemile.R;
import de.glassbox.bluemile.model.CarPart;
import de.glassbox.bluemile.persistence.ModelBuilder;
import de.glassbox.bluemile.ui.adapters.CarPartsAdapter;
import roboguice.fragment.RoboFragment;
import roboguice.inject.InjectView;

public class CarPartsFragment extends RoboFragment {

    @InjectView(R.id.list) ListView mListView;
    private static final int[] resources = new int[]{R.raw.test_tire, R.raw.test_engine, R.raw.test_extra};

    ArrayAdapter<CarPart> mAdapter;
    int section;

    public CarPartsFragment() {
    }

    static CarPartsFragment newInstance(int position) {
        CarPartsFragment fragment = new CarPartsFragment();
        Bundle bundle = new Bundle();
        bundle.putInt("section", position);
        fragment.setArguments(bundle);

        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        section = getArguments().getInt("section");
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_carpart_list, container, false);
    }

    @Override
    public void onViewCreated(View view, Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        try {
            Resources res = getResources();
            InputStream inStream = res.openRawResource(resources[section]);

            byte[] bytes = new byte[inStream.available()];
            inStream.read(bytes);

            List<CarPart> carParts = new ArrayList<CarPart>();
            carParts.add(ModelBuilder.createModelFromJSON(new String(bytes), CarPart.class));
            carParts.add(ModelBuilder.createModelFromJSON(new String(bytes), CarPart.class));
            carParts.add(ModelBuilder.createModelFromJSON(new String(bytes), CarPart.class));
            carParts.add(ModelBuilder.createModelFromJSON(new String(bytes), CarPart.class));
            mAdapter = new CarPartsAdapter(getActivity(), carParts);
            mListView.setAdapter(mAdapter);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }


}
