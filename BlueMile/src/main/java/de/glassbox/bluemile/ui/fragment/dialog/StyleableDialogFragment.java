package de.glassbox.bluemile.ui.fragment.dialog;

import android.os.Bundle;
import android.os.Handler;
import android.support.v4.app.DialogFragment;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentActivity;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.google.inject.Inject;

import de.glassbox.bluemile.R;
import de.glassbox.bluemile.persistence.DataStore;
import roboguice.fragment.RoboDialogFragment;

/**
 * Created by Juli on 16.05.2014.
 */
public class StyleableDialogFragment extends RoboDialogFragment {

    /* Services */
    @Inject
    protected DataStore mDataStore;
    @Inject
    protected Handler mHandler;

    public void createPopup(FragmentActivity activity, boolean addToBackStack) {
        createPopup(activity.getSupportFragmentManager(), addToBackStack);
    }

    public void createPopup(Fragment containerFragment, boolean addToBackStack) {
        createPopup(containerFragment.getFragmentManager(), addToBackStack);
    }

    private void createPopup(FragmentManager fragmentManager, boolean addToBackStack) {
        FragmentTransaction ft = fragmentManager.beginTransaction();

        if (addToBackStack) {
            Fragment prev = fragmentManager.findFragmentByTag("dialog");
            if (prev != null) {
                ft.remove(prev);
            }
            ft.addToBackStack(null);
        }

        this.show(ft, "dialog");
        this.setCancelable(true);
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        // set dialog style
        int style = DialogFragment.STYLE_NO_TITLE, theme = R.style.RoundDialogStyle;
        setStyle(style, theme);
    }

    protected View onCreateView(LayoutInflater inflater, ViewGroup container, int layout, Bundle savedInstanceState) {
        ViewGroup v = (ViewGroup) inflater.inflate(layout, container, false);
        v.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                StyleableDialogFragment.this.dismiss();
            }
        });

        v.getChildAt(0).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                // prevents clickthrough
            }
        });

        return v;
    }

    protected void createInfoPopup(String headline, String description, int icon) {
        // Create and show the dialog.
        final StyleableDialogFragment newFragment = InfoDialogFragment.newInstance(headline, description, icon);
        newFragment.createPopup(this, false);
        newFragment.setTargetFragment(getTargetFragment(), getTargetRequestCode());

        mHandler.postDelayed(new Runnable() {
            @Override
            public void run() {
                if (newFragment != null) {
                    newFragment.dismiss();
                }
            }
        }, 5000);

        this.dismiss();
    }

    @Override
    public void onStart() {
        super.onStart();

        // safety check
        if (getDialog() == null) {
            super.setShowsDialog(false);
            return;
        }

        // override layout params so that the dialog matches the parent completely
        getDialog().getWindow().setLayout(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.MATCH_PARENT);
    }
}
