package de.glassbox.bluemile.ui.adapters;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import de.glassbox.bluemile.R;

/**
 * Created by Juli on 21.06.2014.
 */
public class SidebarAdapter extends ArrayAdapter<SidebarItem> {

    public SidebarAdapter(Context context) {
        super(context, 0);
    }

    public View getView(int position, View convertView, ViewGroup parent) {
        if (convertView == null) {
            convertView = LayoutInflater.from(getContext()).inflate(R.layout.sidebar_item, null);
        }

        SidebarItem item = getItem(position);
        TextView title = (TextView) convertView.findViewById(R.id.sidebar_item_text);
        title.setText(item.text);

        ImageView imageView = (ImageView) convertView.findViewById(R.id.sidebar_item_icon);
        imageView.setImageResource(item.iconRes);
        imageView.setBackgroundColor(item.iconColorRes);

        return convertView;
    }


}
