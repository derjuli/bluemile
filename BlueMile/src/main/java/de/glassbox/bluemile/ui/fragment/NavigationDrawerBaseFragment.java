package de.glassbox.bluemile.ui.fragment;


import android.app.Activity;
import android.app.Fragment;
import android.os.Bundle;

import de.glassbox.bluemile.ui.callback.ChangeDrawerTitle;
import roboguice.fragment.RoboFragment;

/**
 * A simple {@link Fragment} subclass.
 */
public class NavigationDrawerBaseFragment extends RoboFragment {

    /**
     * The fragment argument representing the section number for this
     * fragment.
     */
    private static final String ARG_SECTION_NUMBER = "section_number";

    public NavigationDrawerBaseFragment() {
        // Required empty public constructor
    }

    /**
     * Returns a new instance of this fragment for the given section
     * number.
     */
    protected NavigationDrawerBaseFragment setArguments(int sectionNumber) {
        Bundle args = new Bundle();
        args.putInt(ARG_SECTION_NUMBER, sectionNumber);
        this.setArguments(args);
        return this;
    }

    @Override
    public void onAttach(Activity activity) {
        super.onAttach(activity);
        if (activity instanceof ChangeDrawerTitle) {
            ((ChangeDrawerTitle) activity).onSectionAttached(getArguments().getInt(ARG_SECTION_NUMBER));
        }
    }
}
