package de.glassbox.bluemile.ui.fragment;


import android.os.Bundle;
import android.support.v4.app.FragmentTransaction;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Spinner;

import de.glassbox.bluemile.R;
import roboguice.inject.InjectView;

public class LeaderboardSelectionFragment extends NavigationDrawerBaseFragment implements NavigationDrawerFragment.NavigationDrawerCallbacks, AdapterView.OnItemSelectedListener {

    // request codes we use when invoking an external activity
    final int RC_RESOLVE = 5000, RC_UNUSED = 5001;
    @InjectView(R.id.spinner_leaderboards_points) Spinner mSpinnerPoints;
    @InjectView(R.id.spinner_leaderboards_type) Spinner mSpinnerType;
    @InjectView(R.id.spinner_leaderboards_time) Spinner mSpinnerTime;

    public LeaderboardSelectionFragment() {
    }

    public static NavigationDrawerBaseFragment newInstance(int section) {
        NavigationDrawerBaseFragment fragment = new LeaderboardSelectionFragment();
        return fragment.setArguments(section);
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        // restore or create fragment
        if (savedInstanceState == null) {
        } else {
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_leaderboard_selection, container, false);
    }

    @Override
    public void onViewCreated(View view, Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        ArrayAdapter pointsAdapter = new ArrayAdapter<String>(getActivity(), android.R.layout.simple_list_item_1, android.R.id.text1,
            getResources().getStringArray(R.array.leaderboard_filters_points));
        mSpinnerPoints.setAdapter(pointsAdapter);
        mSpinnerPoints.setOnItemSelectedListener(this);

        ArrayAdapter typeAdapter = new ArrayAdapter<String>(getActivity(), android.R.layout.simple_list_item_1, android.R.id.text1,
            getResources().getStringArray(R.array.leaderboard_filters_type));
        mSpinnerType.setAdapter(typeAdapter);
        mSpinnerType.setOnItemSelectedListener(this);

        ArrayAdapter timeAdapter = new ArrayAdapter<String>(getActivity(), android.R.layout.simple_list_item_1, android.R.id.text1,
            getResources().getStringArray(R.array.leaderboard_filters_time));
        mSpinnerTime.setAdapter(timeAdapter);
        mSpinnerTime.setOnItemSelectedListener(this);

//        GoogleApiClient client = ((GoogleApiProvider) getActivity()).getGoogleApiClient();
//        PendingResult<Leaderboards.LeaderboardMetadataResult> result = Games.Leaderboards.loadLeaderboardMetadata(client, true);
//        result.setResultCallback(new ResultCallback<Leaderboards.LeaderboardMetadataResult>() {
//            @Override
//            public void onResult(Leaderboards.LeaderboardMetadataResult leaderboardMetadataResult) {
//                createLeaderboardPicker(leaderboardMetadataResult.getLeaderboards());
//            }
//        });

//        if (((BaseGameActivity) getActivity()).isSignedIn()) {
//        startActivityForResult(Games.Leaderboards.getAllLeaderboardsIntent(((GoogleApiProvider) getActivity()).getGoogleApiClient()), RC_UNUSED);
//                } else {
//                        //showAlert(getString(R.string.leaderboards_not_available));
//                }
    }

    private void loadLeaderboard() {
        createScoreboard(getLeaderboardId(), Math.max(0, mSpinnerTime.getSelectedItemPosition()), 20); // daily is standard selection
    }

    private String getLeaderboardId() {
        int points = mSpinnerPoints.getSelectedItemPosition();
        int type = mSpinnerType.getSelectedItemPosition();
        if (points == 0 && type == 0) {
            return getString(R.string.blue_total_id);
        }
        if (points == 0 && type == 1) {
            return getString(R.string.blue_avg_id);
        }
        if (points == 1 && type == 0) {
            return getString(R.string.red_total_id);
        }
        if (points == 1 && type == 1) {
            return getString(R.string.red_avg_id);
        }
        if (points == 2 && type == 0) {
            return getString(R.string.combined_total_id);
        }
        if (points == 2 && type == 1) {
            return getString(R.string.combined_avg_id);
        }
        return getString(R.string.blue_total_id); // standard selection
    }

    private void createScoreboard(String leaderboardId, int span, int limit) {
        FragmentTransaction t = getFragmentManager().beginTransaction();
        t.replace(R.id.content, LeaderboardFragment.newInstance(leaderboardId, span, limit));
        t.commit();
    }

    @Override
    public void onNavigationDrawerItemSelected(int position) {

    }

    @Override
    public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
        loadLeaderboard();
    }

    @Override
    public void onNothingSelected(AdapterView<?> parent) {
    }
}
