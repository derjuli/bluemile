package de.glassbox.bluemile.ui.views;

import android.content.Context;
import android.content.res.TypedArray;
import android.graphics.Bitmap;
import android.graphics.Typeface;
import android.graphics.drawable.BitmapDrawable;
import android.graphics.drawable.Drawable;
import android.text.Html;
import android.util.AttributeSet;
import android.widget.TextView;

import org.apache.commons.lang3.StringUtils;

import de.glassbox.bluemile.R;
import de.glassbox.bluemile.utils.Typefaces;

/**
 * Improved {@link android.widget.TextView} that can have a custom font applied.
 *
 * @author Julian Sievers
 * @version 1.0
 */
public class TypefaceTextView extends TextView {
    private boolean mResizeDrawable;
    private boolean mRichText;

    public TypefaceTextView(final Context context) {
        this(context, null);
    }

    public TypefaceTextView(final Context context, final AttributeSet attrs) {
        this(context, attrs, 0);
    }

    public TypefaceTextView(final Context context, final AttributeSet attrs, final int defStyle) {
        super(context, attrs, defStyle);

        final TypedArray a = context.obtainStyledAttributes(attrs, R.styleable.TypefaceTextView);
        final String font = a.getString(R.styleable.TypefaceTextView_customFont);

        if (!isInEditMode()) {
            setCustomFont(context, font);
        }

        mResizeDrawable = a.getBoolean(R.styleable.TypefaceTextView_resizeDrawable, false);
        mRichText = a.getBoolean(R.styleable.TypefaceTextView_richText, false);
        if (a.getBoolean(R.styleable.TypefaceTextView_onlyXmlText, false)) {
            setText(getText());
        }

        a.recycle();
    }

    //    @Override
    //    protected void onMeasure(int widthMeasureSpec, int heightMeasureSpec) {
    //        if (mResizeDrawable) {
    //            Drawable[] drawables = getCompoundDrawables();
    //            float height = getTextSize() / getLineHeight() * getTextSize();
    //            //height *= 2f;
    //            if (drawables != null && drawables.length > 0) {
    //                for (int i = 0; i < drawables.length; i++) {
    //                    if (drawables[i] != null) {
    //                        drawables[i] = resize(drawables[i], (int) (height / drawables[i].getIntrinsicHeight() * drawables[i].getIntrinsicWidth()), (int) height);
    //                    }
    //                }
    //                setCompoundDrawablesWithIntrinsicBounds(drawables[0], drawables[1], drawables[2], drawables[3]);
    //            }
    //        }
    //
    //        super.onMeasure(widthMeasureSpec, heightMeasureSpec);
    //    }

    private Drawable resize(Drawable image, int width, int height) {
        Bitmap b = ((BitmapDrawable) image).getBitmap();
        Bitmap bitmapResized = Bitmap.createScaledBitmap(b, width, height, true);
        return new BitmapDrawable(getResources(), bitmapResized);
    }

    @Override
    public void setText(CharSequence text, BufferType type) {
        super.setText(mRichText ? Html.fromHtml(text.toString()) : text, type);
    }

    /**
     * Loads a given font from the assets folder and applies the font to the TextView.
     *
     * @param context   The current context.
     * @param fontAsset The font asset to load.
     */
    public void setCustomFont(final Context context, String fontAsset) {
        if (StringUtils.isEmpty(fontAsset)) {
            return;
        }

        Typeface tf = Typefaces.get(context, fontAsset);
        if (tf == null) {
            return;
            //            fontAsset = getResources().getString(R.string.font_standard);
            //            tf = Typefaces.get(context, fontAsset);
        }

        setTypeface(tf);
    }

}