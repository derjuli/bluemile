package de.glassbox.bluemile.ui.fragment;


import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import de.glassbox.bluemile.R;

public class StoreFragment extends NavigationDrawerBaseFragment {

    public static NavigationDrawerBaseFragment newInstance(int section) {
        NavigationDrawerBaseFragment fragment = new StoreFragment();
        return fragment.setArguments(section);
    }

    public StoreFragment() {
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_leaderboard_selection, container, false);
    }


}
