package de.glassbox.bluemile.ui.fragment;

import android.graphics.Color;
import android.os.AsyncTask;
import android.os.Bundle;
import android.util.Log;

import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.GooglePlayServicesUtil;
import com.google.android.gms.maps.CameraUpdate;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.MapsInitializer;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.UiSettings;
import com.google.android.gms.maps.model.BitmapDescriptorFactory;
import com.google.android.gms.maps.model.CameraPosition;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.LatLngBounds;
import com.google.android.gms.maps.model.Marker;
import com.google.android.gms.maps.model.MarkerOptions;
import com.google.android.gms.maps.model.PolylineOptions;

import java.util.ArrayList;
import java.util.List;

import de.glassbox.bluemile.R;
import de.glassbox.bluemile.model.BMLocation;

/**
 * Created by Juli on 02.05.2014.
 */
public class CustomMapFragment extends SupportMapFragment {

    private static final String TAG = CustomMapFragment.class.getSimpleName();

    private List<BMLocation> mLocationsToDraw;
    private BMLocation mLastLocation;
    private Marker mUserMarker;
    private boolean mShowUserLocation;
    private boolean mShowStartEndMarkers;

    public BMLocation getLastLocation() {
        return mLastLocation;
    }

    public void setUserLocation(BMLocation lastLocation) {
        this.mLastLocation = lastLocation;
        zoomCameraToUserLocation(setUserLocation());
    }

    public void setShowUserLocation(boolean showUserLocation) {
        this.mShowUserLocation = showUserLocation;
    }

    public void setShowStartEndMarkers(boolean showStartEndMarkers) {
        this.mShowStartEndMarkers = showStartEndMarkers;
    }

    public synchronized void setLocationsToDraw(List<BMLocation> locationsToDraw) {
        this.mLocationsToDraw = locationsToDraw;
    }

    @Override
    public void onActivityCreated(Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);

        if (getMap() != null) {
            MapsInitializer.initialize(getActivity().getApplicationContext());
            // The Map is verified. It is now safe to manipulate the map.
            // remove overlays from maps
            UiSettings settings = getMap().getUiSettings();
            settings.setMyLocationButtonEnabled(false);
            settings.setZoomControlsEnabled(false);
            settings.setAllGesturesEnabled(false);

            getMap().setOnMapLoadedCallback(new GoogleMap.OnMapLoadedCallback() {
                @Override
                public void onMapLoaded() {
                    zoomCameraToUserLocation(setUserLocation());
                    if (mLocationsToDraw != null && mLocationsToDraw.size() > 0) {
                        drawLine(new ArrayList<BMLocation>(mLocationsToDraw));
                        mLocationsToDraw = null;
                    }
                }
            });
        } else {
            int checkGooglePlayServices = GooglePlayServicesUtil.isGooglePlayServicesAvailable(getActivity());
            if (checkGooglePlayServices != ConnectionResult.SUCCESS) {
                // google play services is missing!!!!
              /*
               * Returns status code indicating whether there was an error.
               * Can be one of following in ConnectionResult: SUCCESS,
               * SERVICE_MISSING, SERVICE_VERSION_UPDATE_REQUIRED,
               * SERVICE_DISABLED, SERVICE_INVALID.
               */
                GooglePlayServicesUtil.getErrorDialog(checkGooglePlayServices, getActivity(), 123);
            }
        }
    }

    private LatLng setUserLocation() {
        if (mShowUserLocation && mLastLocation != null && getMap() != null) {
            LatLng latLng = new LatLng(mLastLocation.getLatitude(), mLastLocation.getLongitude());

            if (mUserMarker == null) {
                MarkerOptions options = new MarkerOptions()
                    .position(latLng)
                    .anchor(0.5f, 0.5f)
                    .flat(true)
                    .icon(BitmapDescriptorFactory.fromResource(R.drawable.map_indicator));
                mUserMarker = getMap().addMarker(options);
            } else {
                mUserMarker.setPosition(latLng);
            }

            return latLng;
        }

        return null;
    }

    private void zoomCameraToUserLocation(LatLng location) {
        if (location != null) {
            CameraPosition cameraPosition = new CameraPosition.Builder().target(location).zoom(14.0f).build();
            CameraUpdate cameraUpdate = CameraUpdateFactory.newCameraPosition(cameraPosition);
            getMap().moveCamera(cameraUpdate);
        }
    }

    public void drawLine(final List<BMLocation> locations) {
        if (locations != null && locations.size() > 0) {
            new PolylineTask().execute(locations);
        }
    }

    private void drawStartEndMarker(LatLng start, LatLng end) {
        if (mShowStartEndMarkers && start != null && end != null) {
            drawMarker(start, R.drawable.map_start);
            drawMarker(end, R.drawable.map_finish);
        }
    }

    private void drawMarker(LatLng point, int drawableResource) {
        MarkerOptions markerOptions = new MarkerOptions()
            .position(point)
            .flat(true)
            .icon(BitmapDescriptorFactory.fromResource(drawableResource));
        getMap().addMarker(markerOptions);
    }

    private static class PolylineContainer {
        PolylineOptions polylineOptions;
        LatLngBounds latLngBounds;
        LatLng startLocation;
        LatLng endLocation;
    }

    private class PolylineTask extends AsyncTask<List<BMLocation>, Void, PolylineContainer> {

        @Override
        protected PolylineContainer doInBackground(List<BMLocation>... locations) {
            Log.d(TAG, String.format("building polyline from %d locations", locations[0].size()));

            final PolylineOptions options = new PolylineOptions();
            options.width(2);
            options.color(Color.BLUE);
            //options.geodesic(true);
            LatLngBounds.Builder builderOfBounds = new LatLngBounds.Builder();
            if (mLastLocation != null) {
                locations[0].add(0, mLastLocation);
            }

            for (BMLocation location : locations[0]) {
                if (location != null) {
                    LatLng pos = new LatLng(location.getLatitude(), location.getLongitude());
                    options.add(pos);
                    builderOfBounds.include(pos);

                    // has to be set every time because unfortunately there can be null locations in our list
                    // so the last element is not known reliably. we need the last location so the the next polyline can be drawn seeminglessly.
                    mLastLocation = location;
                }
            }

            // create the bounds so that the map can zoom to a level where the complete track is visible
            final LatLngBounds bounds = builderOfBounds.build();
            PolylineContainer polylineContainer = new PolylineContainer();
            polylineContainer.polylineOptions = options;
            polylineContainer.latLngBounds = builderOfBounds.build();

            List<LatLng> points = options.getPoints();
            if (points.size() > 1) {
                polylineContainer.startLocation = points.get(0);
                polylineContainer.endLocation = points.get(points.size() - 1);
            }

            return polylineContainer;
        }

        @Override
        protected void onPostExecute(PolylineContainer polylineContainer) {
            Log.d(TAG, "drawing polyline on map");

            getMap().addPolyline(polylineContainer.polylineOptions);
            getMap().animateCamera(CameraUpdateFactory.newLatLngBounds(polylineContainer.latLngBounds, 80));
            setUserLocation();
            drawStartEndMarker(polylineContainer.startLocation, polylineContainer.endLocation);
        }
    }
}
