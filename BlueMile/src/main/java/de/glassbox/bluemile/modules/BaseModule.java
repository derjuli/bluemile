package de.glassbox.bluemile.modules;

import com.google.inject.AbstractModule;
import com.google.inject.Singleton;
import com.squareup.otto.Bus;

import de.glassbox.bluemile.model.MainThreadBus;
import de.glassbox.bluemile.rest.RequestHandler;


/**
 * @author Julian Sievers
 * @version $Id: $
 */
public class BaseModule extends AbstractModule {

    public BaseModule() {
        super();
    }

    @Override
    protected void configure() {
        bind(RequestHandler.class).in(Singleton.class);
        bind(Bus.class).to(MainThreadBus.class).in(Singleton.class);
    }

}