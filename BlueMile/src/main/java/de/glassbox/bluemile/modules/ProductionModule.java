package de.glassbox.bluemile.modules;

import android.util.Log;

/**
 * @author Julian Sievers
 * @version $Id: $
 */
public final class ProductionModule extends BaseModule {

    public ProductionModule() {
        super();
        Log.d(ProductionModule.class.getSimpleName(), "Creating Production Module");
    }

    @Override
    protected void configure() {
        super.configure();

    }

}