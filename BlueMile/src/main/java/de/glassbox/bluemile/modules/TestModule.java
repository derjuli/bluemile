package de.glassbox.bluemile.modules;

import android.util.Log;

import com.google.inject.Singleton;

import de.glassbox.bluemile.services.MockLocationProvider;

/**
 * @author Julian Sievers
 * @version $Id: $
 */
public final class TestModule extends BaseModule {

    public TestModule() {
        Log.d(TestModule.class.getSimpleName(), "Creating Test Module");
    }

    @Override
    protected void configure() {
        super.configure();
        bind(MockLocationProvider.class).in(Singleton.class);
    }

}