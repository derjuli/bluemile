package de.glassbox.bluemile.persistence;

import android.content.Context;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;

import com.google.inject.Inject;

/**
 * Created by Juli on 19.02.14.
 */
public class DatabaseHelper extends SQLiteOpenHelper {

    private static final int DATABASE_VERSION = 1;
    private static final String DATABASE_NAME = "bluemile.db";

    public static final String TABLE_LOCATION = "location";
    public static final String TABLE_TRACK_RESULT = "track_result";

    public static final String COLUMN_TRACK_RESULT_LOCAL_ID = "localId";
    public static final String COLUMN_TRACK_RESULT_ID = "id";
    public static final String COLUMN_TRACK_RESULT_TIMESTAMP = "timestamp"; // can be used for paging
    public static final String COLUMN_TRACK_RESULT_DATA = "data";
    public static final String COLUMN_TRACK_RESULT_SYNCED = "synced";

    public static final String COLUMN_LOCATION_ID = "loc_id";
    public static final String COLUMN_LOCATION_TRACK_ID = "trackID"; // needed for join
    public static final String COLUMN_LOCATION_DATA = "data"; // actual data
    public static final String COLUMN_LOCATION_SYNCED = "synced"; // true if already synced
    public static final String COLUMN_LOCATION_TIMESTAMP = "timestamp"; // needed for sorted select (select 100 oldest locations)


    // Database creation sql statement
    // @formatter:off

    private static final String DATABASE_CREATE_TRACK_RESULT = "create table IF NOT EXISTS " + TABLE_TRACK_RESULT + "("
            + COLUMN_TRACK_RESULT_LOCAL_ID + " INTEGER primary key autoincrement,"
            + COLUMN_TRACK_RESULT_ID + " INTEGER,"
            + COLUMN_TRACK_RESULT_TIMESTAMP + " INTEGER,"
            + COLUMN_TRACK_RESULT_SYNCED + " INTEGER,"
            + COLUMN_TRACK_RESULT_DATA + " TEXT)";

    private static final String DATABASE_CREATE_LOCATION = "create table IF NOT EXISTS " + TABLE_LOCATION +"("
            + COLUMN_LOCATION_ID + " INTEGER primary key autoincrement,"
            + COLUMN_LOCATION_SYNCED + " INTEGER,"
            + COLUMN_LOCATION_TIMESTAMP + " INTEGER,"
            + COLUMN_LOCATION_DATA + " TEXT,"
            + COLUMN_LOCATION_TRACK_ID + " INTEGER)";

    // @formatter:on

    /**
     * Constructor
     * Takes and keeps a reference of the passed context in order to access to the application assets and resources.
     *
     * @param context
     */
    @Inject
    public DatabaseHelper(Context context) {
        super(context, DATABASE_NAME, null, DATABASE_VERSION);
    }

    @Override
    public void onCreate(SQLiteDatabase database) {
        database.execSQL(DATABASE_CREATE_LOCATION);
        database.execSQL(DATABASE_CREATE_TRACK_RESULT);
    }

    @Override
    public void onUpgrade(SQLiteDatabase database, int oldVersion, int newVersion) {

    }

}
