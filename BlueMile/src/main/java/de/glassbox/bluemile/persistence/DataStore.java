package de.glassbox.bluemile.persistence;

import com.google.inject.Singleton;

import de.glassbox.bluemile.model.User;

/**
 * Created by Juli on 07.02.14.
 * <p/>
 * Used for passing complex data between activities and fragments.
 * Only store short lasting data here since static variables can be killed by the os very fast.
 */
@Singleton
public class DataStore {

    private User mUser;

    public User getUser() {
        return mUser;
    }

    public void setUser(User user) {
        this.mUser = user;
    }

}
