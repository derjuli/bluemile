package de.glassbox.bluemile.persistence;

import android.text.TextUtils;
import android.util.Log;

import com.fasterxml.jackson.annotation.JsonAutoDetect;
import com.fasterxml.jackson.core.JsonParseException;
import com.fasterxml.jackson.core.JsonParser;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.DeserializationFeature;
import com.fasterxml.jackson.databind.JsonMappingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.introspect.VisibilityChecker;
import com.fasterxml.jackson.databind.type.TypeFactory;

import org.json.JSONException;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

/**
 * Utility class to deserialize JSON string into classes.
 *
 * @author Julian Sievers
 * @version 1.0
 */
public class ModelBuilder {

    private static final String TAG = ModelBuilder.class.getSimpleName();

    private static ObjectMapper mapper;

    static {
        mapper = new ObjectMapper().configure(DeserializationFeature.FAIL_ON_UNKNOWN_PROPERTIES, false).configure(JsonParser.Feature.ALLOW_UNQUOTED_FIELD_NAMES, true);
        mapper.setVisibilityChecker(VisibilityChecker.Std.defaultInstance().withFieldVisibility(JsonAutoDetect.Visibility.ANY));
    }

    /**
     * Takes a JSON string and deserializes it into the given class.
     *
     * @param json  The JSON string.
     * @param model The class to instantiate and populate.
     * @return Instance of the given class with all data from the json string.
     */
    public static <T> T createModelFromJSON(final String json, final Class<T> model) throws JSONException {
        try {
            if (!TextUtils.isEmpty(json)) {
                return mapper.readValue(json, model);
            }
        } catch (final JsonParseException ex) {
            Log.e(TAG, ex.getMessage());
        } catch (final JsonMappingException ex) {
            Log.e(TAG, ex.getMessage());
        } catch (final IOException ex) {
            Log.e(TAG, ex.getMessage());
        }

        throw new JSONException("Error creating " + model.getSimpleName() + " from JSON String " + json);
    }

    /**
     * Takes a JOSN string and deserializes it into the given class.
     *
     * @param json  The JSON string.
     * @param model The class to instantiate and populate.
     * @return Instance of the given class with all data from the json string.
     */
    public static <T> List<T> createListModelFromJSON(final String json, Class<T> model) {
        List<T> deserialized = null;

        try {
            if (!TextUtils.isEmpty(json)) {
                TypeFactory t = TypeFactory.defaultInstance();
                deserialized = mapper.readValue(json, t.constructCollectionType(ArrayList.class, model));
            }
        } catch (final JsonParseException ex) {
            Log.e(TAG, ex.getMessage());
        } catch (final JsonMappingException ex) {
            Log.e(TAG, ex.getMessage());
        } catch (final IOException ex) {
            Log.e(TAG, ex.getMessage());
        }

        return deserialized == null ? new ArrayList<T>() : deserialized;
    }

    /**
     * Serializes a given object to a JSON string.
     *
     * @param object The object to serialize.
     * @return The JSON string of the object.
     */
    public static String objectToJSON(Object object) {
        try {
            return mapper.writeValueAsString(object);
        } catch (JsonProcessingException e) {
            e.printStackTrace();
        }
        return "";
    }
}
