package de.glassbox.bluemile.persistence;

import android.content.Context;
import android.database.sqlite.SQLiteDatabase;

import com.google.inject.Inject;

/**
 * Created by Juli on 21.02.14.
 */
public class MockDatabaseHelper extends DatabaseHelper {

    private Context mContext;
    private static final boolean DROP_ON_OPEN = false;

    /**
     * Constructor
     * Takes and keeps a reference of the passed context in order to access to the application assets and resources.
     *
     * @param context
     */
    @Inject
    public MockDatabaseHelper(Context context) {
        super(context);
        this.mContext = context;
    }

    @Override
    public void onCreate(SQLiteDatabase database) {
        super.onCreate(database);

        // load sample road and write it to the database
        //        String json = AndroidUtils.readStringFromFile(mContext, R.raw.road_test);
        //        ContentValues values = new ContentValues();
        //        values.put(COLUMN_ROAD_DATA, json);
        //
        //        // insert row
        //        long roadID = database.insertWithOnConflict(TABLE_ROAD, null, values, SQLiteDatabase.CONFLICT_REPLACE);
    }

    @Override
    public void onOpen(SQLiteDatabase db) {
        super.onOpen(db);

        if (DROP_ON_OPEN) {
            // on open drop older tables
            db.execSQL("DROP TABLE IF EXISTS " + TABLE_LOCATION);
            db.execSQL("DROP TABLE IF EXISTS " + TABLE_TRACK_RESULT);
        }

        this.onCreate(db);
    }
}
