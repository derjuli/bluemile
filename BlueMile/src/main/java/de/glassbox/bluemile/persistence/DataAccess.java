package de.glassbox.bluemile.persistence;

import android.content.ContentValues;
import android.content.Context;
import android.content.SharedPreferences;
import android.database.Cursor;
import android.database.SQLException;
import android.database.sqlite.SQLiteDatabase;
import android.os.Handler;
import android.os.Looper;
import android.util.Log;
import android.util.SparseArray;

import com.google.inject.Inject;
import com.google.inject.Singleton;

import org.json.JSONException;

import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.Callable;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.Future;

import de.glassbox.bluemile.model.BMLocation;
import de.glassbox.bluemile.model.BMTrackResult;
import de.glassbox.bluemile.model.BMUser;
import de.glassbox.bluemile.model.User;


/**
 * Created by Juli on 19.02.14.
 */
@Singleton
public class DataAccess {
    public interface DeliverResult<T> {
        void onResult(T data);
    }

    private static final String TAG = DataAccess.class.getSimpleName();
    private static final String PREFS_NAME = "BlueMilePrefs";

    /* Services */
    private DatabaseHelper mDBHelper;
    private SQLiteDatabase mDatabase;
    private final ExecutorService mExecutor;

    /* Data */
    private final SparseArray<Future<?>> mTaskStore;
    private final Handler handler = new Handler(Looper.getMainLooper());

    @Inject
    public DataAccess(DatabaseHelper dbHelper) {
        mExecutor = Executors.newFixedThreadPool(5);
        mTaskStore = new SparseArray<Future<?>>();

        this.mDBHelper = dbHelper;
        this.open();
    }

    public void open() throws SQLException {
        mDatabase = mDBHelper.getWritableDatabase();
    }

    public void close() {
        cancelLoadingAll();
        mDBHelper.close();
    }

    // load
    private <T> void loadAsync(Callable<T> callable, final DeliverResult<T> callback, boolean postOnUI) {
        final Future<?> future = mExecutor.submit(callable);
        mTaskStore.put(future.hashCode(), future);

        try {
            final T result = (T) future.get();
            // return result on ui thread if needed
            if (callback == null) {
                return;
            }
            if (postOnUI) {
                handler.post(new Runnable() {
                    @Override
                    public void run() {
                        callback.onResult(result);
                    }
                });
            } else {
                callback.onResult(result);
            }

        } catch (InterruptedException e) {
            e.printStackTrace();
        } catch (ExecutionException e) {
            e.printStackTrace();
        }
    }

    public void cancelLoadingAll() {
        for (int i = mTaskStore.size() - 1; i >= 0; i--) {
            mTaskStore.valueAt(i).cancel(true);
            mTaskStore.removeAt(i);
        }
    }

    public void getLocationsAsync(final long trackID, final DeliverResult<List<BMLocation>> callback, boolean postOnUI) {
        loadAsync(new Callable<List<BMLocation>>() {
            @Override
            public List<BMLocation> call() throws Exception {
                return getLocations(trackID, Integer.MAX_VALUE);
            }
        }, callback, postOnUI);
    }

    public List<BMLocation> getLocations(long localTrackId, int limit) {
        // @formatter: off
        String oldestUnsyncedLocations = " SELECT " + DatabaseHelper.COLUMN_LOCATION_ID + "," + DatabaseHelper.COLUMN_LOCATION_DATA +
                " FROM " + DatabaseHelper.TABLE_LOCATION +
                " WHERE " + DatabaseHelper.COLUMN_LOCATION_TRACK_ID + "=" + localTrackId + " AND " + DatabaseHelper.COLUMN_LOCATION_SYNCED + "=0" +
                " ORDER BY " + DatabaseHelper.COLUMN_LOCATION_TIMESTAMP + " ASC" +
                " LIMIT " + limit;
        // @formatter:on

        Cursor cursor = mDatabase.rawQuery(oldestUnsyncedLocations, null);

        if (cursor.moveToFirst()) {
            final List<BMLocation> list = new ArrayList<BMLocation>(cursor.getCount());
            do {
                String json = cursor.getString(1);
                BMLocation location;
                try {
                    location = ModelBuilder.createModelFromJSON(json, BMLocation.class);
                } catch (JSONException e) {
                    e.printStackTrace();
                    continue;
                }
                location.setId(cursor.getLong(0));
                list.add(location);
            } while (cursor.moveToNext());
            return list;
        }

        return new ArrayList<BMLocation>();
    }

    public void getTrackResultsAsync(final DeliverResult<List<BMTrackResult>> callback, boolean postOnUI) {
        loadAsync(new Callable<List<BMTrackResult>>() {
            @Override
            public List<BMTrackResult> call() throws Exception {
                return getTrackResults(false);
            }
        }, callback, postOnUI);
    }

    public BMTrackResult getTrackResultById(long id) {
        Cursor cursor = mDatabase.query(DatabaseHelper.TABLE_TRACK_RESULT, new String[]{DatabaseHelper.COLUMN_TRACK_RESULT_DATA,
                        DatabaseHelper.COLUMN_TRACK_RESULT_LOCAL_ID}, DatabaseHelper.COLUMN_TRACK_RESULT_LOCAL_ID + "=?", new String[]{String.valueOf(id)}, null, null, null
        );

        List<BMTrackResult> trackResults = createTrackListFromCursor(cursor);
        return trackResults.size() > 0 ? trackResults.get(0) : null;
    }


    /**
     * Loads all track results from the database.
     *
     * @return List with all track results. Empty list if nothing was found.
     */
    private List<BMTrackResult> getTrackResults(boolean synced) {
        Cursor cursor = mDatabase.query(DatabaseHelper.TABLE_TRACK_RESULT, new String[]{DatabaseHelper.COLUMN_TRACK_RESULT_DATA,
                        DatabaseHelper.COLUMN_TRACK_RESULT_LOCAL_ID, DatabaseHelper.COLUMN_LOCATION_TIMESTAMP}, synced ? DatabaseHelper.COLUMN_TRACK_RESULT_SYNCED : null,
                (synced ? new String[]{String.valueOf(0)} : null), null, null, DatabaseHelper.COLUMN_LOCATION_TIMESTAMP + " DESC"
        );

        return createTrackListFromCursor(cursor);
    }

    private List<BMTrackResult> createTrackListFromCursor(Cursor cursor) {
        final List<BMTrackResult> list = new ArrayList<BMTrackResult>();
        if (cursor.moveToFirst()) {
            do {
                String json = cursor.getString(0);
                try {
                    BMTrackResult trackResult = ModelBuilder.createModelFromJSON(json, BMTrackResult.class);
                    trackResult.setLocalId(cursor.getLong(1));
                    list.add(trackResult);
                } catch (JSONException ex) {
                    Log.e(TAG, ex.getMessage());
                    return list;
                }
            } while (cursor.moveToNext());
        }
        return list;
    }

    public void storeTrackResultAsync(final BMTrackResult trackResult, DeliverResult<BMTrackResult> callback, boolean postOnUI) {
        loadAsync(new Callable<BMTrackResult>() {
            @Override
            public BMTrackResult call() throws Exception {
                return storeTrackResult(trackResult);
            }
        }, callback, postOnUI);
    }

    public BMTrackResult storeTrackResult(BMTrackResult trackResult) {
        ContentValues values = new ContentValues();
        if (trackResult.getLocalId() != 0) {
            values.put(DatabaseHelper.COLUMN_TRACK_RESULT_LOCAL_ID, trackResult.getLocalId());
        }
        values.put(DatabaseHelper.COLUMN_TRACK_RESULT_ID, trackResult.getId());
        values.put(DatabaseHelper.COLUMN_TRACK_RESULT_SYNCED, trackResult.getTrackStatusEnum() == BMTrackResult.TrackStatus.Finished);
        values.put(DatabaseHelper.COLUMN_TRACK_RESULT_TIMESTAMP, trackResult.getStartTime().getTime());
        values.put(DatabaseHelper.COLUMN_TRACK_RESULT_DATA, ModelBuilder.objectToJSON(trackResult));

        long id = mDatabase.insertWithOnConflict(DatabaseHelper.TABLE_TRACK_RESULT, null, values, SQLiteDatabase.CONFLICT_REPLACE);
        trackResult.setLocalId(id);
        Log.d(TAG, "inserting new track result: " + id);
        return trackResult;
    }

    public User getUser(Context context) {
        SharedPreferences preferencesReader = context.getSharedPreferences(PREFS_NAME, Context.MODE_PRIVATE);
        try {
            return ModelBuilder.createModelFromJSON(preferencesReader.getString("User", null), BMUser.class);
        } catch (JSONException ex) {
            Log.e(TAG, ex.getMessage());
            return null;
        }
    }

    public void storeUser(Context context, User user) {
        // Save the serialized data into a shared preference
        SharedPreferences preferencesReader = context.getSharedPreferences(PREFS_NAME, Context.MODE_PRIVATE);
        SharedPreferences.Editor editor = preferencesReader.edit();
        editor.putString("User", ModelBuilder.objectToJSON(user));
        editor.commit();
    }

    public void deleteUser(Context context) {
        // Save the serialized data into a shared preference
        SharedPreferences preferencesReader = context.getSharedPreferences(PREFS_NAME, Context.MODE_PRIVATE);
        SharedPreferences.Editor editor = preferencesReader.edit();
        editor.putString("User", null);
        editor.commit();
    }


    public void storeLocationAsync(final BMLocation location, final long localTrackId) {
        Log.d(TAG, "storing location");
        mExecutor.submit(new Runnable() {
            @Override
            public void run() {
                ContentValues values = new ContentValues();
                values.put(DatabaseHelper.COLUMN_LOCATION_TRACK_ID, localTrackId);
                values.put(DatabaseHelper.COLUMN_LOCATION_SYNCED, 0);
                values.put(DatabaseHelper.COLUMN_LOCATION_TIMESTAMP, location.getTimestamp() != null ? location.getTimestamp().getTime() : System.currentTimeMillis());
                values.put(DatabaseHelper.COLUMN_LOCATION_DATA, ModelBuilder.objectToJSON(location));

                Log.d(TAG, "inserting new location");
                mDatabase.insert(DatabaseHelper.TABLE_LOCATION, null, values);
            }
        });
    }
}
