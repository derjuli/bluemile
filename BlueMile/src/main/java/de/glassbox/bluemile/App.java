package de.glassbox.bluemile;

import android.app.Application;

import com.nostra13.universalimageloader.core.ImageLoader;
import com.nostra13.universalimageloader.core.ImageLoaderConfiguration;

import java.util.Random;

import de.glassbox.bluemile.modules.ProductionModule;
import de.glassbox.bluemile.modules.TestModule;
import de.glassbox.bluemile.utils.AndroidUtils;
import roboguice.RoboGuice;

/**
 * @author Julian Sievers
 * @version 1.0
 */
public class App extends Application {

    private static final String TAG = "Application Class";

    public static Random sRng = new Random();
    public static boolean sSkipIntro = true;
    public static boolean sDebuggable = false;
    private static boolean sLogging = false;
    private static boolean sPrintKeyHash = false;

    @Override
    public void onCreate() {
        super.onCreate();

        //sDebuggable = AndroidUtils.isDebuggable(getApplicationContext());

        if (sPrintKeyHash) {
            AndroidUtils.printAppKeyHash(getApplicationContext(), "de.glassbox.bluemile");
        }

        // Create global configuration and initialize ImageLoader with this configuration
        ImageLoaderConfiguration config = new ImageLoaderConfiguration.Builder(getApplicationContext()).build();
        ImageLoader.getInstance().init(config);

        // init DI
        if (sDebuggable) {
            RoboGuice.setBaseApplicationInjector(this, RoboGuice.DEFAULT_STAGE, RoboGuice.newDefaultRoboModule(this), new TestModule());
        } else {
            RoboGuice.setBaseApplicationInjector(this, RoboGuice.DEFAULT_STAGE, RoboGuice.newDefaultRoboModule(this), new ProductionModule());
        }
    }

}