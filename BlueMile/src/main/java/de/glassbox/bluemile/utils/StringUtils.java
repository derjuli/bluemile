package de.glassbox.bluemile.utils;

/**
 * Created by Juli on 06.07.2014.
 */
public class StringUtils {

    public static String getNumberSuffix(long number) {
        int n = (int) (number % 100);
        if (n >= 11 && n <= 13) {
            return "th";
        }
        switch (n % 10) {
            case 1:
                return "st";
            case 2:
                return "nd";
            case 3:
                return "rd";
            default:
                return "th";
        }
    }
}
