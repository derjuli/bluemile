package de.glassbox.bluemile.utils;

import android.content.Context;
import android.graphics.Typeface;
import android.util.Log;

import java.util.Hashtable;

/**
 * Utility class for {@link android.graphics.Typeface}s to avoid memory leaks by holding a set of already used fonts
 * so that they do not have to be recreated on every usage.
 *
 * @author Julian Sievers
 * @version 1.0
 */
public class Typefaces {
    private static final String TAG = "Typefaces";

    private static final Hashtable<String, Typeface> cache = new Hashtable<String, Typeface>();

    /**
     * Loads a given font from the assets folder and caches it.
     *
     * @param context  The current context.
     * @param fontPath The font asset to load.
     * @return True if applying was successful, false on error.
     */
    public static Typeface get(final Context context, String fontPath) {
        synchronized (cache) {
            if (!cache.containsKey(fontPath)) {
                try {
                    final Typeface t = Typeface.createFromAsset(context.getAssets(), fontPath);
                    cache.put(fontPath, t);
                } catch (final Exception e) {
                    Log.e(TAG, "Could not get typeface '" + fontPath + "' because " + e.getMessage());
                    return null;
                }
            }
            return cache.get(fontPath);
        }
    }
}