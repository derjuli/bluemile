package de.glassbox.bluemile.utils;

import android.graphics.Bitmap;

import com.nostra13.universalimageloader.core.DisplayImageOptions;
import com.nostra13.universalimageloader.core.assist.ImageScaleType;

/**
 * Created by Juli on 02.05.2014.
 */
public class ImageLoaderUtils {

    private static DisplayImageOptions mInMemory;
    private static DisplayImageOptions mOnDisc;

    /**
     * Creates basic image loading options.
     *
     * @return Basic image loading Options
     */
    public static DisplayImageOptions getOnDiscOptions() {
        if (mInMemory == null) {
            mInMemory = new DisplayImageOptions.Builder().cacheInMemory(false).cacheOnDisc(true).bitmapConfig(Bitmap.Config.RGB_565)
                //                .showStubImage(R.drawable.image_stub)
                //.resetViewBeforeLoading(true)
                .imageScaleType(ImageScaleType.NONE).build();
        }
        return mInMemory;
    }

    /**
     * Creates basic image loading options.
     *
     * @return Basic image loading Options
     */
    public static DisplayImageOptions getInMemoryOptions() {
        if (mOnDisc == null) {
            mOnDisc = new DisplayImageOptions.Builder().cacheInMemory(true).cacheOnDisc(true)
                //.displayer(new FadeInBitmapDisplayer(375))
                .resetViewBeforeLoading(true).bitmapConfig(Bitmap.Config.RGB_565).imageScaleType(ImageScaleType.NONE).build();
        }
        return mOnDisc;
    }
}
