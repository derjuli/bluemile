package de.glassbox.bluemile.utils;

import android.graphics.Bitmap;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.PorterDuff;

/**
 * @author Julian Sievers
 * @version 1.0
 */
public class ImageUtils {

    private static final int colorStep = 7;

    /**
     * Changes the bitmaps opacity.
     *
     * @param bitmap  The bitmap to change.
     * @param opacity The final opacity of the bitmap.
     * @return New Bitmap instance with the given opacity.
     */
    public static Bitmap adjustColor(final Bitmap bitmap, final int opacity) {
        //make sure bitmap is mutable (copy of needed)
        final Bitmap mutableBitmap = bitmap.isMutable() ? bitmap : bitmap.copy(Bitmap.Config.ARGB_8888, true);

        //draw the bitmap into a canvas
        final Canvas canvas = new Canvas(mutableBitmap);

        //create a color with the specified opacity
        final int colour = Color.argb(opacity, 0, 0, 0);

        //draw the colour over the bitmap using PorterDuff mode DST_IN
        canvas.drawColor(colour, PorterDuff.Mode.DST_IN);

        //now return the adjusted bitmap
        return mutableBitmap;
    }

    /**
     * Darkens the given color depending on the level. The higher the level, the darker the color.
     *
     * @param color The color as #AARRGGBB
     * @param level The level.
     * @return Manipulated color as #AARRGGBB
     */
    public static int adjustColorLevel(int color, int level) {
        int r = Math.max(Color.red(color) - colorStep * level, 0);
        int g = Math.max(Color.green(color) - colorStep * level, 0);
        int b = Math.max(Color.blue(color) - colorStep * level, 0);

        return Color.argb(Color.alpha(color), r, g, b);
    }

    /**
     * Converts a hex string to an int color.
     *
     * @param colorStr e.g. "#FFFFFF"
     * @return Converted color as int.
     */
    public static int hex2Rgb(String colorStr) {
        colorStr = colorStr.replace("#", "");
        return Color.rgb(Integer.valueOf(colorStr.substring(0, 2), 16), Integer.valueOf(colorStr.substring(2, 4), 16), Integer.valueOf(colorStr.substring(4, 6), 16));
    }

    /**
     * Converts an int color to a hex string.
     *
     * @param color Color as int.
     * @return Converted color as hex e.g. "#FFFFFF"
     */
    public static String rgb2Hex(int color) {
        return String.format("%06X", (0xFFFFFF & color));
    }

}
