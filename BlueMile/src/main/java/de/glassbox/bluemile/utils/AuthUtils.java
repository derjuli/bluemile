package de.glassbox.bluemile.utils;

import android.util.Base64;

import java.util.HashMap;

/**
 * Created by Juli on 12.06.2014.
 */
public class AuthUtils {

    public static HashMap buildAuthHeaders(String username, String password) {
        HashMap<String, String> params = new HashMap<String, String>();
        String creds = String.format("%s:%s", username, password);
        String auth = "Basic " + Base64.encodeToString(creds.getBytes(), Base64.NO_WRAP);
        params.put("Authorization", auth);
        return params;
    }

}
